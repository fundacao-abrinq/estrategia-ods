/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/javascript/functionalities/archive.js":
/*!******************************************************!*\
  !*** ./assets/javascript/functionalities/archive.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

jQuery(function ($) {
  var canBeLoaded = true,
      bottomOffset = 2000;
  var firstLoad = true;
  var selectedCategoryCount = -1;
  var postList = $('.ajax-posts .posts-list');
  var postsContainer = postList.find('.posts-list--content');
  var initialTotal = $('.total > strong').text();
  var totalElement = $('.total > strong');
  var counterElement = $('.actual-number > strong');

  if (!postList.length) {
    return;
  }

  $(postsContainer).bind('DOMSubtreeModified', function () {
    // if(postsContainer.children().length != 0) {
    counterElement.text(postsContainer.children().length); // }
  });
  var data = {
    'action': 'loadmore',
    'query': ods_params.posts,
    'page': ods_params.current_page
  };
  $('select#sort-options').change(function () {
    var processedArgs = JSON.parse(ods_params.posts);

    if ($(this).val() == 'title') {
      processedArgs['orderby'] = 'title';
      processedArgs['order'] = 'ASC';
    } else {
      processedArgs['orderby'] = 'date';
      processedArgs['order'] = 'DESC';
    }

    ods_params.posts = JSON.stringify(processedArgs);
    ods_params.current_page = 0;
    postsContainer.html('');
    buildPosts(ods_params, {
      'action': 'loadmore',
      'query': ods_params.posts,
      'page': ods_params.current_page
    }, postList, false);
  });
  $(window).on("load scroll", function () {
    if ($(document).scrollTop() > $(document).height() - bottomOffset && canBeLoaded == true || firstLoad) {
      var processedArgs = JSON.parse(ods_params.posts);
      firstLoad = false;

      if (processedArgs.posts_per_page * ods_params.current_page < selectedCategoryCount || selectedCategoryCount < 0) {
        buildPosts(ods_params, {
          'action': 'loadmore',
          'query': ods_params.posts,
          'page': ods_params.current_page
        }, postList);
      }
    }
  });
  $('.ajax-posts .filtro-posts .categories > a').click(function () {
    var processedArgs = JSON.parse(ods_params.posts);
    var categoryId = parseInt($(this).attr('data-id'));
    var itensCount = parseInt($(this).attr('data-count'));
    var taxonomy = $(this).attr('data-taxonomy');
    selectedCategoryCount = categoryId > 0 ? itensCount : -1;

    if (taxonomy != 'category') {
      var args = [{
        'taxonomy': taxonomy,
        'terms': categoryId > 0 ? categoryId : "all"
      }];
      processedArgs['tax_query'] = args;
    } else {
      processedArgs['category__in'] = categoryId > 0 ? categoryId : "";
    }

    ods_params.posts = JSON.stringify(processedArgs);
    ods_params.current_page = 0;
    totalElement.text(selectedCategoryCount >= 0 ? selectedCategoryCount : initialTotal);
    $('.ajax-posts .filtro-posts .categories > a').removeClass('active');
    $(this).toggleClass('active');
    postsContainer.html(''); // console.log(ods_params);

    buildPosts(ods_params, {
      'action': 'loadmore',
      'query': ods_params.posts,
      'page': ods_params.current_page
    }, postList, false);
  });

  function buildPosts(params, data, postList) {
    var incrementContent = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
    // console.log(params);
    var postsContainer = postList.find('.posts-list--content');
    jQuery.ajax({
      url: params.ajaxurl,
      data: data,
      type: 'POST',
      beforeSend: function beforeSend(xhr) {
        if (params.current_page + 1 >= params.max_page) {
          postList.find('.loading-posts').hide();
        }

        canBeLoaded = false;
        postList.find('.loading-posts').addClass('active');
      },
      success: function success(data) {
        if (data) {
          // console.log(data)
          if (incrementContent) {
            postsContainer.html(postsContainer.html() + data);
          } else {
            // console.log(data)
            postsContainer.html(data);
          }

          document.location.hash = 'page:' + params.current_page;
          params.current_page++;
          canBeLoaded = true;
          postList.find('.loading-posts').removeClass('active');
        }
      }
    });
  }
});

/***/ }),

/***/ "./assets/scss/app.scss":
/*!******************************!*\
  !*** ./assets/scss/app.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***********************************************************************************!*\
  !*** multi ./assets/javascript/functionalities/archive.js ./assets/scss/app.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/isaquemelo/Documents/estrategia-ods/themes/ods/assets/javascript/functionalities/archive.js */"./assets/javascript/functionalities/archive.js");
module.exports = __webpack_require__(/*! /home/isaquemelo/Documents/estrategia-ods/themes/ods/assets/scss/app.scss */"./assets/scss/app.scss");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2phdmFzY3JpcHQvZnVuY3Rpb25hbGl0aWVzL2FyY2hpdmUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3Njc3MvYXBwLnNjc3M/MGMzZCJdLCJuYW1lcyI6WyJqUXVlcnkiLCIkIiwiY2FuQmVMb2FkZWQiLCJib3R0b21PZmZzZXQiLCJmaXJzdExvYWQiLCJzZWxlY3RlZENhdGVnb3J5Q291bnQiLCJwb3N0TGlzdCIsInBvc3RzQ29udGFpbmVyIiwiZmluZCIsImluaXRpYWxUb3RhbCIsInRleHQiLCJ0b3RhbEVsZW1lbnQiLCJjb3VudGVyRWxlbWVudCIsImxlbmd0aCIsImJpbmQiLCJjaGlsZHJlbiIsImRhdGEiLCJvZHNfcGFyYW1zIiwicG9zdHMiLCJjdXJyZW50X3BhZ2UiLCJjaGFuZ2UiLCJwcm9jZXNzZWRBcmdzIiwiSlNPTiIsInBhcnNlIiwidmFsIiwic3RyaW5naWZ5IiwiaHRtbCIsImJ1aWxkUG9zdHMiLCJ3aW5kb3ciLCJvbiIsImRvY3VtZW50Iiwic2Nyb2xsVG9wIiwiaGVpZ2h0IiwicG9zdHNfcGVyX3BhZ2UiLCJjbGljayIsImNhdGVnb3J5SWQiLCJwYXJzZUludCIsImF0dHIiLCJpdGVuc0NvdW50IiwidGF4b25vbXkiLCJhcmdzIiwicmVtb3ZlQ2xhc3MiLCJ0b2dnbGVDbGFzcyIsInBhcmFtcyIsImluY3JlbWVudENvbnRlbnQiLCJhamF4IiwidXJsIiwiYWpheHVybCIsInR5cGUiLCJiZWZvcmVTZW5kIiwieGhyIiwibWF4X3BhZ2UiLCJoaWRlIiwiYWRkQ2xhc3MiLCJzdWNjZXNzIiwibG9jYXRpb24iLCJoYXNoIl0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7QUNsRkFBLE1BQU0sQ0FBQyxVQUFTQyxDQUFULEVBQVc7QUFDakIsTUFBSUMsV0FBVyxHQUFHLElBQWxCO0FBQUEsTUFDSUMsWUFBWSxHQUFHLElBRG5CO0FBR0csTUFBSUMsU0FBUyxHQUFHLElBQWhCO0FBQ0EsTUFBSUMscUJBQXFCLEdBQUcsQ0FBQyxDQUE3QjtBQUNBLE1BQU1DLFFBQVEsR0FBR0wsQ0FBQyxDQUFDLHlCQUFELENBQWxCO0FBQ0EsTUFBTU0sY0FBYyxHQUFHRCxRQUFRLENBQUNFLElBQVQsQ0FBYyxzQkFBZCxDQUF2QjtBQUNBLE1BQU1DLFlBQVksR0FBR1IsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJTLElBQXJCLEVBQXJCO0FBQ0EsTUFBTUMsWUFBWSxHQUFHVixDQUFDLENBQUMsaUJBQUQsQ0FBdEI7QUFDQSxNQUFNVyxjQUFjLEdBQUdYLENBQUMsQ0FBQyx5QkFBRCxDQUF4Qjs7QUFFQSxNQUFHLENBQUNLLFFBQVEsQ0FBQ08sTUFBYixFQUFxQjtBQUNqQjtBQUNIOztBQUVEWixHQUFDLENBQUNNLGNBQUQsQ0FBRCxDQUFrQk8sSUFBbEIsQ0FBdUIsb0JBQXZCLEVBQTZDLFlBQVU7QUFDbkQ7QUFDSUYsa0JBQWMsQ0FBQ0YsSUFBZixDQUFvQkgsY0FBYyxDQUFDUSxRQUFmLEdBQTBCRixNQUE5QyxFQUYrQyxDQUduRDtBQUNILEdBSkQ7QUFNQSxNQUFNRyxJQUFJLEdBQUc7QUFDVCxjQUFVLFVBREQ7QUFFVCxhQUFTQyxVQUFVLENBQUNDLEtBRlg7QUFHVCxZQUFTRCxVQUFVLENBQUNFO0FBSFgsR0FBYjtBQU1BbEIsR0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJtQixNQUF6QixDQUFnQyxZQUFXO0FBQ3ZDLFFBQU1DLGFBQWEsR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdOLFVBQVUsQ0FBQ0MsS0FBdEIsQ0FBdEI7O0FBRUEsUUFBR2pCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXVCLEdBQVIsTUFBaUIsT0FBcEIsRUFBNkI7QUFDekJILG1CQUFhLENBQUMsU0FBRCxDQUFiLEdBQTJCLE9BQTNCO0FBQ0FBLG1CQUFhLENBQUMsT0FBRCxDQUFiLEdBQXlCLEtBQXpCO0FBQ0gsS0FIRCxNQUdPO0FBQ0hBLG1CQUFhLENBQUMsU0FBRCxDQUFiLEdBQTJCLE1BQTNCO0FBQ0FBLG1CQUFhLENBQUMsT0FBRCxDQUFiLEdBQXlCLE1BQXpCO0FBQ0g7O0FBRURKLGNBQVUsQ0FBQ0MsS0FBWCxHQUFtQkksSUFBSSxDQUFDRyxTQUFMLENBQWVKLGFBQWYsQ0FBbkI7QUFDQUosY0FBVSxDQUFDRSxZQUFYLEdBQTBCLENBQTFCO0FBRUFaLGtCQUFjLENBQUNtQixJQUFmLENBQW9CLEVBQXBCO0FBRUFDLGNBQVUsQ0FBQ1YsVUFBRCxFQUFhO0FBQ25CLGdCQUFVLFVBRFM7QUFFbkIsZUFBU0EsVUFBVSxDQUFDQyxLQUZEO0FBR25CLGNBQVNELFVBQVUsQ0FBQ0U7QUFIRCxLQUFiLEVBSVBiLFFBSk8sRUFJRyxLQUpILENBQVY7QUFLSCxHQXJCRDtBQXVCSEwsR0FBQyxDQUFDMkIsTUFBRCxDQUFELENBQVVDLEVBQVYsQ0FBYSxhQUFiLEVBQTRCLFlBQVU7QUFDckMsUUFBSzVCLENBQUMsQ0FBQzZCLFFBQUQsQ0FBRCxDQUFZQyxTQUFaLEtBQTRCOUIsQ0FBQyxDQUFDNkIsUUFBRCxDQUFELENBQVlFLE1BQVosS0FBdUI3QixZQUFuRCxJQUFxRUQsV0FBVyxJQUFJLElBQXJGLElBQStGRSxTQUFuRyxFQUE2RztBQUNuRyxVQUFNaUIsYUFBYSxHQUFHQyxJQUFJLENBQUNDLEtBQUwsQ0FBV04sVUFBVSxDQUFDQyxLQUF0QixDQUF0QjtBQUNBZCxlQUFTLEdBQUcsS0FBWjs7QUFFQSxVQUFHaUIsYUFBYSxDQUFDWSxjQUFkLEdBQStCaEIsVUFBVSxDQUFDRSxZQUExQyxHQUF5RGQscUJBQXpELElBQWtGQSxxQkFBcUIsR0FBRyxDQUE3RyxFQUFnSDtBQUM1R3NCLGtCQUFVLENBQUNWLFVBQUQsRUFBYTtBQUNuQixvQkFBVSxVQURTO0FBRW5CLG1CQUFTQSxVQUFVLENBQUNDLEtBRkQ7QUFHbkIsa0JBQVNELFVBQVUsQ0FBQ0U7QUFIRCxTQUFiLEVBSVBiLFFBSk8sQ0FBVjtBQUtIO0FBRVY7QUFDRSxHQWRKO0FBZ0JHTCxHQUFDLENBQUMsMkNBQUQsQ0FBRCxDQUErQ2lDLEtBQS9DLENBQXFELFlBQVc7QUFDNUQsUUFBTWIsYUFBYSxHQUFHQyxJQUFJLENBQUNDLEtBQUwsQ0FBV04sVUFBVSxDQUFDQyxLQUF0QixDQUF0QjtBQUNBLFFBQU1pQixVQUFVLEdBQU1DLFFBQVEsQ0FBQ25DLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW9DLElBQVIsQ0FBYSxTQUFiLENBQUQsQ0FBOUI7QUFDQSxRQUFNQyxVQUFVLEdBQU1GLFFBQVEsQ0FBQ25DLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW9DLElBQVIsQ0FBYSxZQUFiLENBQUQsQ0FBOUI7QUFDQSxRQUFNRSxRQUFRLEdBQVF0QyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFvQyxJQUFSLENBQWEsZUFBYixDQUF0QjtBQUVBaEMseUJBQXFCLEdBQUc4QixVQUFVLEdBQUcsQ0FBYixHQUFnQkcsVUFBaEIsR0FBNkIsQ0FBQyxDQUF0RDs7QUFFQSxRQUFJQyxRQUFRLElBQUksVUFBaEIsRUFBNEI7QUFDeEIsVUFBSUMsSUFBSSxHQUFHLENBQUM7QUFDUixvQkFBWUQsUUFESjtBQUVSLGlCQUFZSixVQUFVLEdBQUcsQ0FBYixHQUFnQkEsVUFBaEIsR0FBNkI7QUFGakMsT0FBRCxDQUFYO0FBSUFkLG1CQUFhLENBQUMsV0FBRCxDQUFiLEdBQTZCbUIsSUFBN0I7QUFDSCxLQU5ELE1BTU87QUFDSG5CLG1CQUFhLENBQUMsY0FBRCxDQUFiLEdBQWdDYyxVQUFVLEdBQUcsQ0FBYixHQUFnQkEsVUFBaEIsR0FBNkIsRUFBN0Q7QUFDSDs7QUFFRGxCLGNBQVUsQ0FBQ0MsS0FBWCxHQUFtQkksSUFBSSxDQUFDRyxTQUFMLENBQWVKLGFBQWYsQ0FBbkI7QUFDQUosY0FBVSxDQUFDRSxZQUFYLEdBQTBCLENBQTFCO0FBRUFSLGdCQUFZLENBQUNELElBQWIsQ0FBa0JMLHFCQUFxQixJQUFJLENBQXpCLEdBQTRCQSxxQkFBNUIsR0FBb0RJLFlBQXRFO0FBRUFSLEtBQUMsQ0FBQywyQ0FBRCxDQUFELENBQStDd0MsV0FBL0MsQ0FBMkQsUUFBM0Q7QUFDQXhDLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXlDLFdBQVIsQ0FBb0IsUUFBcEI7QUFFQW5DLGtCQUFjLENBQUNtQixJQUFmLENBQW9CLEVBQXBCLEVBMUI0RCxDQTRCNUQ7O0FBQ0FDLGNBQVUsQ0FBQ1YsVUFBRCxFQUFhO0FBQ25CLGdCQUFVLFVBRFM7QUFFbkIsZUFBU0EsVUFBVSxDQUFDQyxLQUZEO0FBR25CLGNBQVNELFVBQVUsQ0FBQ0U7QUFIRCxLQUFiLEVBSVBiLFFBSk8sRUFJRyxLQUpILENBQVY7QUFLSCxHQWxDRDs7QUFxQ0EsV0FBU3FCLFVBQVQsQ0FBb0JnQixNQUFwQixFQUE0QjNCLElBQTVCLEVBQWtDVixRQUFsQyxFQUFxRTtBQUFBLFFBQXpCc0MsZ0JBQXlCLHVFQUFOLElBQU07QUFDakU7QUFFQSxRQUFNckMsY0FBYyxHQUFHRCxRQUFRLENBQUNFLElBQVQsQ0FBYyxzQkFBZCxDQUF2QjtBQUVBUixVQUFNLENBQUM2QyxJQUFQLENBQVk7QUFDUkMsU0FBRyxFQUFHSCxNQUFNLENBQUNJLE9BREw7QUFFUi9CLFVBQUksRUFBRUEsSUFGRTtBQUdSZ0MsVUFBSSxFQUFFLE1BSEU7QUFJUkMsZ0JBQVUsRUFBRSxvQkFBVUMsR0FBVixFQUFlO0FBQ3ZCLFlBQUdQLE1BQU0sQ0FBQ3hCLFlBQVAsR0FBc0IsQ0FBdEIsSUFBMkJ3QixNQUFNLENBQUNRLFFBQXJDLEVBQWdEO0FBQzVDN0Msa0JBQVEsQ0FBQ0UsSUFBVCxDQUFjLGdCQUFkLEVBQWdDNEMsSUFBaEM7QUFDSDs7QUFFRGxELG1CQUFXLEdBQUcsS0FBZDtBQUNBSSxnQkFBUSxDQUFDRSxJQUFULENBQWMsZ0JBQWQsRUFBZ0M2QyxRQUFoQyxDQUF5QyxRQUF6QztBQUNILE9BWE87QUFZUkMsYUFBTyxFQUFFLGlCQUFTdEMsSUFBVCxFQUFjO0FBQ25CLFlBQUlBLElBQUosRUFBVztBQUNQO0FBRUEsY0FBRzRCLGdCQUFILEVBQXFCO0FBQ2pCckMsMEJBQWMsQ0FBQ21CLElBQWYsQ0FBb0JuQixjQUFjLENBQUNtQixJQUFmLEtBQXdCVixJQUE1QztBQUNILFdBRkQsTUFFTztBQUNIO0FBQ0FULDBCQUFjLENBQUNtQixJQUFmLENBQXFCVixJQUFyQjtBQUNIOztBQUVEYyxrQkFBUSxDQUFDeUIsUUFBVCxDQUFrQkMsSUFBbEIsR0FBeUIsVUFBVWIsTUFBTSxDQUFDeEIsWUFBMUM7QUFDQXdCLGdCQUFNLENBQUN4QixZQUFQO0FBRUFqQixxQkFBVyxHQUFHLElBQWQ7QUFFQUksa0JBQVEsQ0FBQ0UsSUFBVCxDQUFjLGdCQUFkLEVBQWdDaUMsV0FBaEMsQ0FBNEMsUUFBNUM7QUFDSDtBQUNKO0FBOUJPLEtBQVo7QUFpQ0g7QUFDSixDQS9JSyxDQUFOLEM7Ozs7Ozs7Ozs7O0FDQUEseUMiLCJmaWxlIjoiL2Rpc3QvYXJjaGl2ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL1wiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG4iLCJqUXVlcnkoZnVuY3Rpb24oJCl7XG5cdHZhciBjYW5CZUxvYWRlZCA9IHRydWUsIFxuXHQgICAgYm90dG9tT2Zmc2V0ID0gMjAwMDtcbiBcbiAgICBsZXQgZmlyc3RMb2FkID0gdHJ1ZTtcbiAgICBsZXQgc2VsZWN0ZWRDYXRlZ29yeUNvdW50ID0gLTE7XG4gICAgY29uc3QgcG9zdExpc3QgPSAkKCcuYWpheC1wb3N0cyAucG9zdHMtbGlzdCcpO1xuICAgIGNvbnN0IHBvc3RzQ29udGFpbmVyID0gcG9zdExpc3QuZmluZCgnLnBvc3RzLWxpc3QtLWNvbnRlbnQnKTtcbiAgICBjb25zdCBpbml0aWFsVG90YWwgPSAkKCcudG90YWwgPiBzdHJvbmcnKS50ZXh0KCk7XG4gICAgY29uc3QgdG90YWxFbGVtZW50ID0gJCgnLnRvdGFsID4gc3Ryb25nJyk7XG4gICAgY29uc3QgY291bnRlckVsZW1lbnQgPSAkKCcuYWN0dWFsLW51bWJlciA+IHN0cm9uZycpO1xuXG4gICAgaWYoIXBvc3RMaXN0Lmxlbmd0aCkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgJChwb3N0c0NvbnRhaW5lcikuYmluZCgnRE9NU3VidHJlZU1vZGlmaWVkJywgZnVuY3Rpb24oKXtcbiAgICAgICAgLy8gaWYocG9zdHNDb250YWluZXIuY2hpbGRyZW4oKS5sZW5ndGggIT0gMCkge1xuICAgICAgICAgICAgY291bnRlckVsZW1lbnQudGV4dChwb3N0c0NvbnRhaW5lci5jaGlsZHJlbigpLmxlbmd0aCk7XG4gICAgICAgIC8vIH1cbiAgICB9KTtcblxuICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICAgICdhY3Rpb24nOiAnbG9hZG1vcmUnLFxuICAgICAgICAncXVlcnknOiBvZHNfcGFyYW1zLnBvc3RzLFxuICAgICAgICAncGFnZScgOiBvZHNfcGFyYW1zLmN1cnJlbnRfcGFnZSxcbiAgICB9O1xuXG4gICAgJCgnc2VsZWN0I3NvcnQtb3B0aW9ucycpLmNoYW5nZShmdW5jdGlvbigpIHtcbiAgICAgICAgY29uc3QgcHJvY2Vzc2VkQXJncyA9IEpTT04ucGFyc2Uob2RzX3BhcmFtcy5wb3N0cyk7XG5cbiAgICAgICAgaWYoJCh0aGlzKS52YWwoKSA9PSAndGl0bGUnKSB7XG4gICAgICAgICAgICBwcm9jZXNzZWRBcmdzWydvcmRlcmJ5J10gPSAndGl0bGUnO1xuICAgICAgICAgICAgcHJvY2Vzc2VkQXJnc1snb3JkZXInXSA9ICdBU0MnO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcHJvY2Vzc2VkQXJnc1snb3JkZXJieSddID0gJ2RhdGUnO1xuICAgICAgICAgICAgcHJvY2Vzc2VkQXJnc1snb3JkZXInXSA9ICdERVNDJztcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgb2RzX3BhcmFtcy5wb3N0cyA9IEpTT04uc3RyaW5naWZ5KHByb2Nlc3NlZEFyZ3MpO1xuICAgICAgICBvZHNfcGFyYW1zLmN1cnJlbnRfcGFnZSA9IDA7XG5cbiAgICAgICAgcG9zdHNDb250YWluZXIuaHRtbCgnJyk7XG5cbiAgICAgICAgYnVpbGRQb3N0cyhvZHNfcGFyYW1zLCB7XG4gICAgICAgICAgICAnYWN0aW9uJzogJ2xvYWRtb3JlJyxcbiAgICAgICAgICAgICdxdWVyeSc6IG9kc19wYXJhbXMucG9zdHMsXG4gICAgICAgICAgICAncGFnZScgOiBvZHNfcGFyYW1zLmN1cnJlbnRfcGFnZSxcbiAgICAgICAgfSwgcG9zdExpc3QsIGZhbHNlKTtcbiAgICB9KTtcblxuXHQkKHdpbmRvdykub24oXCJsb2FkIHNjcm9sbFwiLCBmdW5jdGlvbigpeyAgICAgICAgXG5cdFx0aWYoICgkKGRvY3VtZW50KS5zY3JvbGxUb3AoKSA+ICggJChkb2N1bWVudCkuaGVpZ2h0KCkgLSBib3R0b21PZmZzZXQgKSAmJiBjYW5CZUxvYWRlZCA9PSB0cnVlKSAgfHwgZmlyc3RMb2FkKXtcbiAgICAgICAgICAgIGNvbnN0IHByb2Nlc3NlZEFyZ3MgPSBKU09OLnBhcnNlKG9kc19wYXJhbXMucG9zdHMpO1xuICAgICAgICAgICAgZmlyc3RMb2FkID0gZmFsc2U7XG5cbiAgICAgICAgICAgIGlmKHByb2Nlc3NlZEFyZ3MucG9zdHNfcGVyX3BhZ2UgKiBvZHNfcGFyYW1zLmN1cnJlbnRfcGFnZSA8IHNlbGVjdGVkQ2F0ZWdvcnlDb3VudCB8fCBzZWxlY3RlZENhdGVnb3J5Q291bnQgPCAwKSB7ICAgIFxuICAgICAgICAgICAgICAgIGJ1aWxkUG9zdHMob2RzX3BhcmFtcywge1xuICAgICAgICAgICAgICAgICAgICAnYWN0aW9uJzogJ2xvYWRtb3JlJyxcbiAgICAgICAgICAgICAgICAgICAgJ3F1ZXJ5Jzogb2RzX3BhcmFtcy5wb3N0cyxcbiAgICAgICAgICAgICAgICAgICAgJ3BhZ2UnIDogb2RzX3BhcmFtcy5jdXJyZW50X3BhZ2UsXG4gICAgICAgICAgICAgICAgfSwgcG9zdExpc3QpO1xuICAgICAgICAgICAgfVxuXG5cdFx0fVxuICAgIH0pO1xuICAgIFxuICAgICQoJy5hamF4LXBvc3RzIC5maWx0cm8tcG9zdHMgLmNhdGVnb3JpZXMgPiBhJykuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgICAgIGNvbnN0IHByb2Nlc3NlZEFyZ3MgPSBKU09OLnBhcnNlKG9kc19wYXJhbXMucG9zdHMpO1xuICAgICAgICBjb25zdCBjYXRlZ29yeUlkICAgID0gcGFyc2VJbnQoJCh0aGlzKS5hdHRyKCdkYXRhLWlkJykpO1xuICAgICAgICBjb25zdCBpdGVuc0NvdW50ICAgID0gcGFyc2VJbnQoJCh0aGlzKS5hdHRyKCdkYXRhLWNvdW50JykpO1xuICAgICAgICBjb25zdCB0YXhvbm9teSAgICAgID0gJCh0aGlzKS5hdHRyKCdkYXRhLXRheG9ub215Jyk7XG4gICAgICAgIFxuICAgICAgICBzZWxlY3RlZENhdGVnb3J5Q291bnQgPSBjYXRlZ29yeUlkID4gMD8gaXRlbnNDb3VudCA6IC0xO1xuICAgICAgICBcbiAgICAgICAgaWYgKHRheG9ub215ICE9ICdjYXRlZ29yeScpIHtcbiAgICAgICAgICAgIHZhciBhcmdzID0gW3tcbiAgICAgICAgICAgICAgICAndGF4b25vbXknOiB0YXhvbm9teSxcbiAgICAgICAgICAgICAgICAndGVybXMnICAgOiBjYXRlZ29yeUlkID4gMD8gY2F0ZWdvcnlJZCA6IFwiYWxsXCJcbiAgICAgICAgICAgIH1dO1xuICAgICAgICAgICAgcHJvY2Vzc2VkQXJnc1sndGF4X3F1ZXJ5J10gPSBhcmdzO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcHJvY2Vzc2VkQXJnc1snY2F0ZWdvcnlfX2luJ10gPSBjYXRlZ29yeUlkID4gMD8gY2F0ZWdvcnlJZCA6IFwiXCI7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIG9kc19wYXJhbXMucG9zdHMgPSBKU09OLnN0cmluZ2lmeShwcm9jZXNzZWRBcmdzKTtcbiAgICAgICAgb2RzX3BhcmFtcy5jdXJyZW50X3BhZ2UgPSAwO1xuXG4gICAgICAgIHRvdGFsRWxlbWVudC50ZXh0KHNlbGVjdGVkQ2F0ZWdvcnlDb3VudCA+PSAwPyBzZWxlY3RlZENhdGVnb3J5Q291bnQgOiBpbml0aWFsVG90YWwpO1xuXG4gICAgICAgICQoJy5hamF4LXBvc3RzIC5maWx0cm8tcG9zdHMgLmNhdGVnb3JpZXMgPiBhJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcblxuICAgICAgICBwb3N0c0NvbnRhaW5lci5odG1sKCcnKTtcblxuICAgICAgICAvLyBjb25zb2xlLmxvZyhvZHNfcGFyYW1zKTtcbiAgICAgICAgYnVpbGRQb3N0cyhvZHNfcGFyYW1zLCB7XG4gICAgICAgICAgICAnYWN0aW9uJzogJ2xvYWRtb3JlJyxcbiAgICAgICAgICAgICdxdWVyeSc6IG9kc19wYXJhbXMucG9zdHMsXG4gICAgICAgICAgICAncGFnZScgOiBvZHNfcGFyYW1zLmN1cnJlbnRfcGFnZSxcbiAgICAgICAgfSwgcG9zdExpc3QsIGZhbHNlKTtcbiAgICB9KTtcblxuXG4gICAgZnVuY3Rpb24gYnVpbGRQb3N0cyhwYXJhbXMsIGRhdGEsIHBvc3RMaXN0LCBpbmNyZW1lbnRDb250ZW50ID0gdHJ1ZSkge1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhwYXJhbXMpO1xuXG4gICAgICAgIGNvbnN0IHBvc3RzQ29udGFpbmVyID0gcG9zdExpc3QuZmluZCgnLnBvc3RzLWxpc3QtLWNvbnRlbnQnKTtcblxuICAgICAgICBqUXVlcnkuYWpheCh7XG4gICAgICAgICAgICB1cmwgOiBwYXJhbXMuYWpheHVybCxcbiAgICAgICAgICAgIGRhdGE6IGRhdGEsXG4gICAgICAgICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICAgICAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbiggeGhyICl7XG4gICAgICAgICAgICAgICAgaWYocGFyYW1zLmN1cnJlbnRfcGFnZSArIDEgPj0gcGFyYW1zLm1heF9wYWdlICkge1xuICAgICAgICAgICAgICAgICAgICBwb3N0TGlzdC5maW5kKCcubG9hZGluZy1wb3N0cycpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgY2FuQmVMb2FkZWQgPSBmYWxzZTsgXG4gICAgICAgICAgICAgICAgcG9zdExpc3QuZmluZCgnLmxvYWRpbmctcG9zdHMnKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oZGF0YSl7XG4gICAgICAgICAgICAgICAgaWYoIGRhdGEgKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGRhdGEpXG5cbiAgICAgICAgICAgICAgICAgICAgaWYoaW5jcmVtZW50Q29udGVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zdHNDb250YWluZXIuaHRtbChwb3N0c0NvbnRhaW5lci5odG1sKCkgKyBkYXRhICk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zdHNDb250YWluZXIuaHRtbCggZGF0YSApO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5sb2NhdGlvbi5oYXNoID0gJ3BhZ2U6JyArIHBhcmFtcy5jdXJyZW50X3BhZ2U7XG4gICAgICAgICAgICAgICAgICAgIHBhcmFtcy5jdXJyZW50X3BhZ2UrKztcblxuICAgICAgICAgICAgICAgICAgICBjYW5CZUxvYWRlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICBwb3N0TGlzdC5maW5kKCcubG9hZGluZy1wb3N0cycpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXG4gICAgICAgIH0pO1xuICAgIH1cbn0pO1xuXG4iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=