<?php
get_header();
the_post();

$post_type = get_post_type();

$author_type = 'autor';

// se por ventura um post type tiver uma taxonomia diferente de autor para representar o autor
// if ($post_type == 'coluna') {
//     $author_type = 'colunista';
// }

$authors = guaraci\authors::get($author_type);

$uses_excerpt = post_type_supports(get_post_type(), 'excerpt');

// o "&& has_excerpt()" faz com que o resumo só seja exibido
// quando o campo do excerpt estiver preenchido, se não o
// resumo automático será utilizado.
$show_excerpt = $uses_excerpt /* && has_excerpt() */;

$taxonomy = 'category';
if ('multimedia' == get_post_type()) {
    $taxonomy = 'midia';
}  

?>
<div class="post-content" id="postContent">
    <div class="row">
        <div class="column large-6 large-centered">
            <div id="single-the-title">
                <div class="categories">
                    <span class="card--category-title"><?php echo get_the_term_list($post->ID, $taxonomy, '', ', '); ?></span>
                </div>
                <h1><?php the_title() ?></h1>
            </div>

            <?php if ($show_excerpt) : ?>
                <div id="single-the-excerpt">
                    <div class="post-excerpt"><?php echo ods\get_excerpt(); ?></div>
                </div>
            <?php endif ?>

            <div class="post-info">
                <?php if (!empty($authors)) : ?>
                    <div class="author">Por <?php guaraci\authors::display($author_type) ?> | </div>
                <?php endif ?>
                <time class="date">
                    <?php _e('Publicado em ', 'ods') ?> <?php the_date("d \d\\e F, Y") ?>
                </time>
            </div>

            <div id="single-post-thumbnail">
                <?php if (has_post_thumbnail()) : ?>
                    <?= guaraci\images::tag('full', 'post--image') ?>
                    <span class="thumbnail-caption">
                        <?php the_post_thumbnail_caption() ?>
                    </span>
                <?php endif; ?>
            </div>

            <div id="single-post-content">
                <?php the_content(); ?>
            </div>

            <?php if (has_tag()) : ?>
                <div class="large-12 small-12">
                    <div class="post-content--tags">
                        <span class="post-content--section-title fz-14 mr-15">Tags:</span>
                        <?php the_tags('') ?>
                    </div>
                </div>
            <?php endif; ?>

        </div>

        <div class="column large-12">
            <div class="posts-list list related-posts">
                <?php guaraci\template_part('related-posts') ?>
            </div>
        </div>
    </div>
</div>

<div class="page--share">
    <i class="fas fa-share-alt"></i>
    <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
    <a class="messenger" href="http://www.facebook.com/dialog/send?app_id=346307852736540&link=<?= get_the_permalink() ?>&redirect_uri=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-facebook-messenger"></i></a>

    <a class="twitter" href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()) ?>&url=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-twitter"></i></a>
    <a class="whatsapp hide-for-large" href="whatsapp://send?text=<?= (get_the_title() . ' - ' . get_the_permalink()) ?>" target="_blank"><i class="fab fa-whatsapp"></i></a>
    <a class="whatsapp show-for-large" href="https://api.whatsapp.com/send?text=<?= (get_the_title() . ' - ' . get_the_permalink()) ?>" target="_blank"><i class="fab fa-whatsapp"></i></a>
    <a class="telegram" href="https://telegram.me/share/url?url=<?= get_the_title() . ' - ' . get_the_permalink() ?>" target="_blank"><i class="fab fa-telegram"></i></a>
    <a class="mail" href="mailto:?subject=<?= the_title() ?>&body=<?= get_the_permalink() ?>" target="_blank"><i class="far fa-envelope"></i></a>
</div>
<?php get_footer();
