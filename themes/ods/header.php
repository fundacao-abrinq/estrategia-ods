<!DOCTYPE html>
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1.0">
    <?php wp_head() ?>
    <title><?= is_front_page() ? get_bloginfo('name') : wp_title() ?></title>
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/assets/images/favicon.ico?v=2" />
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
</head>

<body <?php body_class(); ?>>
    <header class="header">
        <div class="row">
            <div class="column large-12 header--content">
                <div class="hamburguer-menu">
                    <button class="menu-btn">
                    </button>
                </div>

                <div class="logo">
                    <a href="/">
                        <?php
                        $custom_logo_id = get_theme_mod('custom_logo');
                        $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                        if (has_custom_logo()) {
                            echo '<img src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '" width="200">';
                        } else {
                        ?>
                            <img src="<?= get_template_directory_uri() ?>/assets/images/logo.png" width="200" alt="<?= get_bloginfo('name') ?>">
                        <?php } ?>
                    </a>
                </div>

                <div class="menu-wrapper">
                    <?php wp_nav_menu(['theme_location' => 'main-menu', 'container' => 'nav', 'menu_id' => 'main-menu', 'menu_class' => 'menu']) ?>
                </div>

                <div class="social-menu">
                    <?php ods\the_social_networks_menu(false) ?>
                </div>

                <div class="search-form">
                    <form action="/">
                        <input type="text" name="s" id="s" placeholder="Digite aqui sua busca...">
                    </form>
                    <button><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </header>
    <div id="app">