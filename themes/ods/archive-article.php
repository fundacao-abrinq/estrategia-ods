<?php
get_header();
$title = '';
?>

<?php guaraci\template_part('featured-header', [
    'title' => 'Artigos',
    'content' => ''
]); ?>

<div class="row">
    <div class="column large-8 medium-7 small-12">
        <?php guaraci\template_part('posts-list', [
            'horizontal'    => false,
            'show_category' => false,
            'show_image'    => true,
            'show_excerpt ' => true,
            'show_author'   => true,
            'show_date'     => true
        ]); ?>
    </div>

    <div class="column large-4 medium-5 small-12 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets', ['sidebar_slug' => 'articles']); ?>
    </div>
</div>

<?php get_footer();
