<?php
get_header();
?>

<div class="row">
    <div class="column large-8 medium-7 small-12">
        <?php guaraci\template_part('search-form') ?>
        <?php guaraci\template_part('posts-list', ['show_category' => false]); ?>
    </div>

    <div class="column large-4 medium-5 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
</div>

<?php get_footer();
