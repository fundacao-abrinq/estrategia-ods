<?php

namespace ods;

function enqueue_assets()
{
    wp_enqueue_style('flickity', 'https://unpkg.com/flickity@2/dist/flickity.min.css');
    wp_enqueue_style('foundation', 'https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation-float.min.css');
    wp_enqueue_style('app', get_template_directory_uri() . '/dist/app.css', [], filemtime(get_template_directory() . '/dist/app.css'));
    wp_enqueue_style('daterangepicker-css', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css', [], '0.1.0', 'all');
    
    wp_enqueue_script('momenta', 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js', ['jquery'], false, true);
    wp_enqueue_script('daterangepicker', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js', ['jquery', 'momenta'], '0.1.0', true);
    wp_enqueue_script('shuffle', get_template_directory_uri() . '/dist/shuffle.min.js', [], false, true);
    wp_enqueue_script('modal', get_template_directory_uri() . '/dist/jquery.modal.min.js', [], false, true);
    wp_enqueue_script('main-app', get_template_directory_uri() . '/dist/app.js', ['jquery'], filemtime(get_template_directory() . '/dist/app.js'), true);
    wp_enqueue_script('cookie', get_template_directory_uri() . '/assets/javascript/js.cookie.js', ['jquery'], false, true);
    wp_enqueue_script('acessibilidade', get_template_directory_uri() . '/assets/javascript/acessibilidade.js', ['jquery', 'cookie'], false, true);
    wp_enqueue_script('youtube-plataform', 'https://apis.google.com/js/platform.js', [], false, true);
    wp_enqueue_script('flickity', 'https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js', ['jquery'], true);
}

add_action('wp_enqueue_scripts', 'ods\\enqueue_assets');

function non_blocking_styles() {
	wp_enqueue_style('fontawesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css", array(), '5.12.0', 'all');
}

add_action('get_footer', 'ods\\non_blocking_styles');

function archive_scripts() {
    global $wp_query; 

    wp_register_script( 'archive_loadmore', get_stylesheet_directory_uri() . '/dist/archive.js', array('jquery'), filemtime(get_template_directory() . '/dist/archive.js'), true );    
    wp_localize_script( 'archive_loadmore', 'ods_params', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
        'posts' => json_encode( $wp_query->query_vars ),
        'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 0,
        'max_page' => $wp_query->max_num_pages
    ) );

    wp_enqueue_script( 'archive_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'ods\\archive_scripts' );