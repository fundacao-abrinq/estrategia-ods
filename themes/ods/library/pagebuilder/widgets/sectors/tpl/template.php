<?php $class = ($instance['header_image']) ? 'has-header-image' : '' ?>

<section class="sectors column large-12 <?= $class ?>">

    <?php if ($instance['header_image']) : ?>

        <div class="sectors--header">
            <img src="<?= wp_get_attachment_url($instance['header_image']) ?>">
        </div>

    <?php endif; ?>

    <div class="sectors--meta">

        <?php if ($instance['use_degrade']) : ?>
            <div class="sectors--meta-icon" style="background: linear-gradient( <?= $instance['base_color'] ?>, #ffffff );">
        <?php else : ?>
            <div class="sectors--meta-icon" style="background-color: <?= $instance['base_color'] ?>;">
        <?php endif; ?>

        <img src="<?= wp_get_attachment_url($instance['icon']) ?>">

        </div><!-- /.sectors--meta-icon -->

        <div class="sectors--title">
            <h2><?= $instance['title'] ?></h2>
        </div>

    </div><!-- /.sectors--meta -->

    <div class="sectors--wrap column large-12">

        <?= apply_filters('the_content', $instance['content']) ?>

        <?php if ( $instance['link'] && $instance['button'] ) :

            if ( strpos( $instance['link'], 'post: ' ) !== false ) {
                $link = str_replace( 'post: ', '', $instance['link'] );
                $link = get_the_permalink( $link );
            } else {
                $link = esc_url( $instance['link'] );
            } ?>

            <a href="<?= esc_url( $link ) ?>" class="btn">
                <?= apply_filters( 'the_title', $instance['button'] ) ?>
            </a>

        <?php endif; ?>

    </div><!-- /.sectors--wrap -->

</section><!-- /.sectors -->