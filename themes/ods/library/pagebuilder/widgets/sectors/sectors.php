<?php
/*
  Widget Name: Setores
  Description: Adiciona blocos para setores (Sociedade Cicil, Governos, etc.)
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class Sectors extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [

            'title' => array(
                'type'  => 'text',
                'label' => __( 'Título', 'ods' ),
            ),
            'header_image' => array(
                'type'  => 'media',
                'label' => __( 'Escolha a imagem do cabeçalho', 'ods' ),
            ),
            'icon' => array(
                'type'    => 'media',
                'label'   => __( 'Ícone', 'ods' ),
                'library' => 'image',
            ),
            'base_color' => array(
                'type'    => 'color',
                'label'   => __( 'Cor fundo do ícone', 'ods' ),
                'default' => '#DE1768'
            ),
            'use_degrade' => array(
                'type'    => 'checkbox',
                'label'   => __( 'Usar degradê no fundo dos ícones?', 'ods' ),
                'default' => true
            ),
            'content' => array(
                'type'  => 'textarea',
                'label' => __( 'Conteúdo', 'ods' ),
            ),
            'button' => array(
                'type'  => 'text',
                'label' => __( 'Título do Botão', 'ods' ),
            ),
            'link' => array(
                'type' => 'link',
                'label' => __( 'Link do Botão', 'ods' ),
            )
            
        ];

        parent::__construct( 'sectors', 'Setores', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description'   => 'Adiciona blocos para setores (Sociedade Cicil, Governos, etc.)'
        ], [], $fields, plugin_dir_path(__FILE__) );
    }

    function get_template_name( $instance ) {
        return 'template';
    }

    function get_style_name( $instance ) {
        return 'style';
    }

}

Siteorigin_widget_register( 'sectors', __FILE__, 'widgets\Sectors' );
