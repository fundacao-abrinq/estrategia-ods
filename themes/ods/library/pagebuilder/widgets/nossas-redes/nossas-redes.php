<?php

/*
  Widget Name: Nossas redes sociais
  Description: Listagem de redes sociais registradas no menu.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class OurSocialNets extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'title' => [
                'type' => 'text',
                'label' => 'Título da seção'
            ],

            'content' => array(
                'type' => 'tinymce',
                'label' => __( 'Conteúdo', 'ods' ),
                'rows' => 10,
                'default_editor' => 'html',
                'button_filters' => array(
                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                ),
            ),

        ];

        parent::__construct('nossas-redes', "Nossas redes sociais", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Listagem de redes sociais registradas no menu.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }


}

Siteorigin_widget_register('nossas-redes', __FILE__, 'widgets\OurSocialNets');
