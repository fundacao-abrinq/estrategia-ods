<div class="our-social-media row">
    <div class="column large-5">
        <h2>
            <?= $instance['title'] ?>
        </h2>

        <div class="content">
            <?= $instance['content'] ?>
        </div>
    </div>

    <div class="column large-7 icons">
        <?php ods\the_social_networks_menu(false) ?>
    </div>
</div>