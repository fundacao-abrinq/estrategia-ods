<section class="slider-section">
    
    <?php foreach ($instance['slider'] as $block) : ?>
        <div class="block carousel-cell slider-item" style="--bg: <?= wp_get_attachment_url($block['background']); ?>;background-image: url(<?= wp_get_attachment_url($block['background']); ?>);">
            <div class="row">
                <div class="column large-5">
                    <div class="title">
                        <h1><?= $block['title'] ?></h1>
                    </div>

                    <div class="content">
                        <?= $block['content'] ?>
                    </div>

                    <?php if (!empty($block['button_text'])) : ?>
                        <a href="<?= $block['button_link'] ?>" <?= $block['new_tab'] ? 'target="_blank"' : '' ?> class="slide-destination">
                            <span><?= $block['button_text'] ?> </span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <button class="arrow-prev">
        <i class="fas fa-arrow-left"></i>
    </button>

    <button class="arrow-next">
        <i class="fas fa-arrow-right"></i>
    </button>

</section>