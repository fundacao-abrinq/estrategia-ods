<?php
/*
  Widget Name: Slider de comprimento completo
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class Slider extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'slider' => array(
                'type' => 'repeater',
                'label' => __( 'Itens' , 'ods'),
                'fields' => array(
                    'title' => array(
                        'type' => 'text',
                        'label' => __( 'Título', 'ods')
                    ),

                    'content' => array(
                        'type' => 'tinymce',
                        'label' => __( 'Conteúdo', 'ods' ),
                        'rows' => 10,
                        'default_editor' => 'html',
                        'button_filters' => array(
                            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                        ),
                    ),

                    'button_text' => array(
                        'type' => 'text',
                        'label' => __( 'Texto do botão', 'ods'),
                        'description' => __( 'Deixe vazio para não criar o botão', 'ods'),
                    ),

                    'button_link' => array(
                        'type' => 'text',
                        'label' => __( 'Destino do botão', 'ods')
                    ),

                    'new_tab' => array(
                        'type' => 'checkbox',
                        'default' => false,
                        'label' => __( 'Abrir link em nova página?', 'ods')
                    ),

                    'background' => array(
                        'type' => 'media',
                        'label' => __('Background', 'ods' ),
                        'library' => 'image',
                    ),                    
                )
            )
        ];

        parent::__construct('slider', 'Slider de comprimento completo', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('slider', __FILE__, 'widgets\Slider');
