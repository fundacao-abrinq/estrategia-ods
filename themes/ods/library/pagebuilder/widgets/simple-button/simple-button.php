<?php

/*
  Widget Name: Botão Simples
  Description: Adicione um botão com o estilo (CSS) do site.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class SimpleButton extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [

            'title' => array(
                'type'  => 'text',
                'label' => __( 'Título do Botão', 'ods' ),
            ),
            'link' => array(
                'type'  => 'link',
                'label' => __('Link do Botão', 'ods'),
            ),
            
        ];

        parent::__construct('simple-button', "Botão Simples", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description'   => 'Adicione um botão com o estilo (CSS) do site.'
            ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('simple-button', __FILE__, 'widgets\SimpleButton');
