<div class="simple-button row">

    <div class="simple-button-wrap column large-12">

        <?php
        if (strpos($instance['link'], 'post: ') !== false ) {
            $link = str_replace('post: ', '', $instance['link']);
            $link = get_the_permalink($link);
        } else {
            $link = esc_url($instance['link']);
        } ?>

        <a href="<?= esc_url($link) ?>" class="btn">
            <?= apply_filters('the_title', $instance['title']) ?>
        </a>

    </div><!-- /.simple-button-wrap -->

</div><!-- /.simple-button -->