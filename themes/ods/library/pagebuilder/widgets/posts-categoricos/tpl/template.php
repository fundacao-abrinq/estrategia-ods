<?php if (!$query_args['tax_query']) : ?>

    <section class="column">
        <div class="content">
            <div class="title">
                <h1><?= $instance['title'] ?></h1>
            </div>
        </div>
    </section>

    <section class="categorical-posts column" remove-repeated="<?= $instance['remove_repeated'] ? 'true' : 'false' ?>">
        <div class="content">
            <div class="search-content posts-list-no-filterable horizontal">

                <?php

                $program_query = new WP_Query($query_args);

                if ($program_query->have_posts()) :
                    while ($program_query->have_posts()) :

                        $program_query->the_post();

                        global $post;
                        guaraci\template_part('card', [
                            'post'          => $post,
                            'horizontal'    => true,
                            'show_category' => false,
                            'show_image'    => true,
                            'show_excerpt ' => true,
                            'show_author'   => false,
                            'show_date'     => true,
                        ]);
                ?>

                <?php endwhile;
                    wp_reset_postdata();
                endif; ?>

            </div>
        </div>

        <?php if (!empty($instance['more'])) : ?>
            <div class="more">
                <a href="<?= $instance['more'] ?>">Ver mais</a>
            </div>
        <?php endif; ?>

    </section>

<?php else : ?>

    <section class="filtro-posts column">
        <div class="content">
            <div class="title">
                <h1><?= $instance['title'] ?></h1>
            </div>

            <div class="categories">
                <a href="#" class="active" data-filter="">
                    Todas
                </a>
            </div>
        </div>
    </section>

    <section class="categorical-posts column" remove-repeated="<?= $instance['remove_repeated'] ? 'true' : 'false' ?>">
        <div class="content">

            <div class="search-content posts-list horizontal">

                <?php
                $categories = get_categories();
                $requested_tax_query = array_slice($query_args['tax_query'], 1);
                $slugs_list = array();

                foreach ($query_args['tax_query'] as $single_term) {
                    if (isset($single_term['taxonomy'])) {
                        $slugs_list[] = $single_term['terms'];
                    }
                }

                $program_query = new WP_Query();

                $posts_array = [];

                foreach ($requested_tax_query as $category) :
                    $filtered_args = $query_args;
                    $filtered_args['posts_per_page'] = 6;
                    $filtered_args['tax_query'] = array('relation' => 'OR') + array($category);

                    $program_query->query($filtered_args);

                    if ($program_query->have_posts()) :
                        while ($program_query->have_posts()) :

                            $program_query->the_post();

                            $post_categories = get_the_category();
                            $filtered_categories = [];

                            foreach ($post_categories as $key => $category) {
                                if (in_array($category->slug, $slugs_list)) {
                                    $filtered_categories[] = $category;
                                }
                            }

                            $post_categories = $filtered_categories;
                            $final_bundle = "";

                            $last_key = end(array_keys($post_categories));
                            foreach ($post_categories as $key => $category) {
                                $final_bundle .= "\"" . $category->name . "\"";
                                $final_bundle .= $key == $last_key ? null : ", ";
                            }
                ?>

                            <?php

                            global $post;

                            if (!in_array(get_the_ID(), $posts_array)) {

                                guaraci\template_part('card', [
                                    'post'          => $post,
                                    'horizontal'    => true,
                                    'show_category' => false,
                                    'show_image'    => true,
                                    'show_excerpt ' => true,
                                    'show_author'   => false,
                                    'show_date'     => true,
                                    'classes'       => 'filterable-card',
                                    'attributes'    => "data-groups      ='[" . $final_bundle . "]'"
                                ]);
                            }

                            $posts_array[] = get_the_ID();

                            ?>

                    <?php endwhile;
                    //wp_reset_postdata();
                    endif; ?>

                <?php endforeach; ?>

            </div>

        </div>

        <?php if (!empty($instance['more'])) : ?>
            <div class="more">
                <a href="<?= $instance['more'] ?>">Ver mais</a>
            </div>
        <?php endif; ?>

    </section>

<?php endif; ?>