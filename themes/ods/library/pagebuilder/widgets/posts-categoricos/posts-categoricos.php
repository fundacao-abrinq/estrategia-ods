<?php
/*
  Widget Name: Posts Filtraveis
  Description: Listagem de posts com suporte a filtragem de posts
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class PostsCategoricos extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [

            'title' => array(
                'type' => 'text',
                'label' => __('Título', 'ods'),
                'default' => 'Últimas notícias'
            ),
            'remove_repeated' => array(
                'type' => 'checkbox',
                'label' => __('Fazer excludente de categorias para obter apenas 1 post de cada na aba "Todos"', 'ods'),
                'default' => true
            ),

            'more' => array(
                'type' => 'text',
                'label' => __('Link ver mais', 'ods'),
                'description' => 'Deixe vazio para não utilizar'
            ),

            'content' => [
                'type' => 'posts',
                'label' => 'Conteúdo'
            ],

            

        ];

        parent::__construct('posts-categoricos', 'Posts Filtraveis', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Listagem de posts com suporte ao widget de filtragem de posts.'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

    function get_template_variables($instance, $args) {
        $posts_query_args = siteorigin_widget_post_selector_process_query($instance['content']);
        return ['query_args' => $posts_query_args];
    }

}

Siteorigin_widget_register('posts-categoricos', __FILE__, 'widgets\PostsCategoricos');
