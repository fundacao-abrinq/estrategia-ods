<div class="posts-tabs">
    <div class="large-12">
        <div class="posts-tabs--header">
            <div class="title">
                <i class="fas fa-palette"></i> <?= $instance['title'] ?>
            </div>
            <div class="subtitle">
                <?= $instance['subtitle'] ?>
            </div>

        </div>

        <div class="posts-tabs--selector-tabs">
            <?php foreach ($instance['tabs'] as $index => $tab) : ?>
                <button target-tab="<?= $index ?>" class="<?= !$index? 'active' : ''?>">
                    <?= $tab['title'] ?>
                </button>
            <?php endforeach; ?>
        </div>

        <div class="posts-tabs--tabs">
            <?php foreach ($instance['tabs'] as $index => $tab) : ?>
                <div class="tab posts-list horizontal <?= !$index? 'active' : ''?>" tab="<?= $index ?>">
                    <?php
                        $posts_query_args = siteorigin_widget_post_selector_process_query($tab['content']);
                        $tab_query = new WP_Query($posts_query_args);
                        

                        if ($tab_query->have_posts()) :
                            while ($tab_query->have_posts()) : ?>
                                <?php $tab_query->the_post(); ?>
                                <?php

                                global $post;
                                guaraci\template_part('card', [
                                    'post' => $post,
                                    'horizontal' => true,
                                    'show_category' => false,
                                    'show_image' => true,
                                    'show_excerpt' => false,
                                    'show_author' => false,
                                    'show_date' => true,
                                ]);

                                ?>
                            <?php endwhile; ?>
                        <?php else: ?>
                            <div class="nothing-found">Não achamos nada aqui ;(</div>

                        <?php endif; ?>

                    <?php if (!empty($tab['more_link'])) : ?>
                        <div class="more">
                            <a href="<?= $tab['more_link'] ?>">
                                ver mais
                            </a>
                        </div>
                    <?php endif; ?>
                </div>

            <?php endforeach; ?>
        </div>

    </div>
</div>