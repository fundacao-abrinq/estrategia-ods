<?php

/*
  Widget Name: Tabs de Posts
  Description: Adiciona bloco de posts com tab trocáveis.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class PostsTabs extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'title' => array(
                'type' => 'text',
                'label' => __('Título', 'ods'),
            ),

            'subtitle' => array(
                'type' => 'textarea',
                'label' => __('Subtitulo', 'ods'),
            ),

            'tabs' => array(
                'type' => 'repeater',
                'label' => __( 'Tabs' , 'ods'),
                'fields' => array(
                    'title' => array(
                        'type' => 'text',
                        'label' => __('Título', 'ods'),
                    ),

                    'content' => [
                        'type' => 'posts',
                        'label' => 'Conteúdo'
                    ],

                    'more_link' => array(
                        'type' => 'text',
                        'label' => __('Link ver mais', 'ods'),
                        'description' => __('Deixe vazio para não utilizar', 'ods'),
                    ),
        
                )
            ),

        ];

        parent::__construct('posts-tabs', "Tabs de Posts", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona bloco de posts com tab trocáveis.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }


}

Siteorigin_widget_register('posts-tabs', __FILE__, 'widgets\PostsTabs');
