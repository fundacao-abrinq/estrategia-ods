<?php

/*
  Widget Name: Newsletter
  Description: Adiciona um bloco de newsletter.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class Newsletter extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'widget_shortcode' => array(
                'type' => 'text',
                'label' => __('Shortcode formulário de contato', 'ods'),
            ),

            'title' => array(
                'type' => 'text',
                'label' => __('Título', 'ods'),
                'default' => 'Newsletter'
            ),

            'subtitle' => array(
                'type' => 'textarea',
                'label' => __('Subtitulo', 'ods'),
            ),
        ];

        parent::__construct('newsletter', "Newsletter", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona um bloco de newsletter.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }


}

Siteorigin_widget_register('newsletter', __FILE__, 'widgets\Newsletter');
