<div class="newsletter">

    <div class="large-12 footer-newsletter">
        <div class="newsletter-header">
            <div class="title">
                <i class="fas fa-envelope"></i> <?= $instance['title'] ?>
            </div>
            <div class="subtitle">
                <?= $instance['subtitle'] ?>
            </div>
            
        </div>

        <div class="form-wrapper">
            <?= do_shortcode($instance['widget_shortcode']) ?>
        </div>
    
    </div>
</div>