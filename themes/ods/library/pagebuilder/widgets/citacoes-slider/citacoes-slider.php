<?php

/*
  Widget Name: Slider de Citações
  Description: Listagem de citações em slider.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class QuoteSlider extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'quotes' => array(
                'type' => 'repeater',
                'label' => __( 'Citações' , 'ods'),
                'fields' => array(
                    'quote' => array(
                        'type' => 'tinymce',
                        'label' => __( 'Conteúdo', 'ods' ),
                        'rows' => 10,
                        'default_editor' => 'html',
                        'button_filters' => array(
                            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                        ),
                    ),

                    'author' => [
                        'type' => 'text',
                        'label' => 'Autor do texto'
                    ],

                    'area' => array(
                        'type' => 'text',
                        'label' => __('O que faz?', 'ods'),
                    ),
        
                )
            ),

        ];

        parent::__construct('citacoes-slider', "Slider de Citações", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Listagem de citações em slider.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }


}

Siteorigin_widget_register('citacoes-slider', __FILE__, 'widgets\QuoteSlider');
