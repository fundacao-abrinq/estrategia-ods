<div class="row">
    <div class="column large-12">
        <section class="quotes-slider">
            <div class="quotes-slider--header-icon">
                <i class="fas fa-quote-right"></i>
            </div>
            <div class="quotes column large-8 large-centered">
                <button class="previous" type="button" aria-label="Previous">
                    <i class="fas fa-arrow-left"></i>
                </button>
                <button class="next" type="button" aria-label="Next">
                    <i class="fas fa-arrow-right"></i>
                </button>

                <?php foreach ($instance['quotes'] as $quote) : ?>
                    <div class="quote">
                        <div class="quote--content">
                            <?= $quote['quote'] ?>
                        </div>

                        <div class="quote--info">
                            <span class="quote--info-author"><b><?= $quote['author'] ?></b>, <?= $quote['area'] ?></span>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
    </div>
</div>