<?php
/*
  Widget Name: Limitador de Grid
  Description: Esse widget limita o seu conteúdo a grid do projeto
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class GridLimiter extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'column_padding' => array(
                'type' => 'checkbox',
                'label' => __( 'Adicionar padding de coluna?', 'widget-form-fields-text-domain'),
            ),

            'page_builder' => array(
                'type' => 'builder',
                'label' => __( 'Page Builder', 'widget-form-fields-text-domain'),
            )
        ];

        parent::__construct('grid-limiter', 'Limitador de Grid', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('grid-limiter', __FILE__, 'widgets\GridLimiter');
