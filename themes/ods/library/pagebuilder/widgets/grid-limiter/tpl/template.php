<div class="row">
    <?= $instance['column_padding']? '<div class="column large-12">' : '' ?>
    <?php 
        $content_builder_id = substr( md5( json_encode( $instance['page_builder'] + [ rand(-100, 100) ] ) ), 0, 8 );
        echo siteorigin_panels_render( 'w'.$content_builder_id, true, $instance['page_builder'] );
    ?>
    <?= $instance['column_padding']? '</div>' : '' ?>
</div>