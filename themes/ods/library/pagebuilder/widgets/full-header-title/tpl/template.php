<div class="full-header-title" style="background-image: url( <?= wp_get_attachment_url($instance['background_image']) ?> );">
    <div class="large-12 full-header-title-wrap">

        <div class="title">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="subtitle">
            <?= $instance['subtitle'] ?>
        </div>

    </div><!-- /.full-header-title-wrap -->
</div><!-- /.full-header-title -->