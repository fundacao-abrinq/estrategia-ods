<?php

/*
  Widget Name: Full Header Title
  Description: Adiciona um cabeçalho lado a lado com título.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class FullHeaderTitle extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [

            'subtitle' => array(
                'type'  => 'textarea',
                'label' => __( 'Subtitulo', 'ods' ),
            ),

            'background_image' => array(
                'type'  => 'media',
                'label' => __( 'Escolha a imagem de fundo', 'ods' ),

            ),
        ];

        parent::__construct('full-header-title', "Cabeçalho com Título", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona um cabeçalho lado a lado com imagem de fundo, título da página e subtítulo.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('full-header-title', __FILE__, 'widgets\FullHeaderTitle');
