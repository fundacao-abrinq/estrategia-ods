<section class="inforative-area">
    <div class="row">
        <div class="column large-4 large-offset-1 image">
            <img src="<?= wp_get_attachment_url($instance['background']) ?>" alt="<?= get_post_meta($instance['background'], '_wp_attachment_image_alt', TRUE) ?>">
        </div>

        <div class="column large-5 content">
            <h4><?= $instance['title'] ?></h4>
            <?= $instance['description'] ?>
        </div>

        <div class="column large-1"></div>
    </div>
    <div class="list">
    <?php 
        $content_builder_id = substr( md5( json_encode( $content ) ), 0, 8 );
        echo siteorigin_panels_render( 'w'.$content_builder_id, true, $instance['page_builder'] );
    ?>
    </div>
</section>