<?php
/*
  Widget Name: Área Informativa
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class InformativeArea extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'title' => array(
                'type' => 'text',
                'label' => __('Título', 'ods' ),
            ),
            'background' => array(
                'type' => 'media',
                'label' => __('Background', 'ods' ),
                'library' => 'image',
            ),
            'description' => array(
                'type' => 'tinymce',
                'label' => __( 'Descrição', 'ods' ),
                'rows' => 10,
                'default_editor' => 'html',
                'button_filters' => array(
                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                ),
            ),
            
            'page_builder' => array(
                'type' => 'builder',
                'label' => __( 'Page Builder', 'widget-form-fields-text-domain'),
            )
        ];

        parent::__construct('informativa-area', 'Área Informativa', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('informativa-area', __FILE__, 'widgets\InformativeArea');
