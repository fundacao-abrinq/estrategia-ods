<?php

/*
  Widget Name: Título e Conteúdo
  Description: Adiciona uma área de conteúdo com título a esquera.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class TitleContent extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [

            'title' => array(
                'type'  => 'text',
                'label' => __( 'Título', 'ods' ),
            ),
            'page_builder' => array(
                'type' => 'builder',
                'label' => __('Page Builder', 'ods'),
            ),
            'background_color' => array(
                'type'    => 'color',
                'label'   => __('Cor fundo do conteúdo', 'ods'),
                'default' => ''
            ),
            
        ];

        parent::__construct('title-content', "Título e Conteúdo", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona uma área de conteúdo com título a esquera.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('title-content', __FILE__, 'widgets\TitleContent');
