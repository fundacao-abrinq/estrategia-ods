<?php if ($instance['background_color']) : ?>
    <div class="title-content row title-content--with-background" style="background-color: <?= $instance['background_color'] ?>">
    <?php else : ?>
        <div class="title-content row">
        <?php endif; ?>

        <div class="title-content-wrap column large-12">

            <div class="large-3">

                <div class="title">
                    <h2><?= $instance['title'] ?></h2>
                </div>

            </div><!-- /.large-3 -->

            <div class="large-9">

                <div class="content">
                    <?php
                    if ( function_exists('siteorigin_panels_render') ) {
                        $content_builder_id = substr(md5(json_encode($instance['page_builder'])), 0, 8);
                        echo siteorigin_panels_render('w' . $content_builder_id, true, $instance['page_builder']);
                    } else {
                        echo __('Este widget precisa do Page Builder instalado.', 'ods');
                    } ?>
                </div><!-- /.content -->

            </div><!-- /.large-9 -->

        </div><!-- /.title-content-wrap -->
        </div><!-- /.title-content -->