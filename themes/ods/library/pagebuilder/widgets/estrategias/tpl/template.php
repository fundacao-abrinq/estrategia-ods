<div class="row estrategia">
    <?php foreach ($instance['estrategias'] as $index => $estrategia) :
        if ($estrategia['use_modal']) { ?>
            <div class="modal estrategia" id="estrategia-<?= $index ?>">
                <div class="modal--wrapper">

                    <div class="modal--header">
                        <div class="featured-image">
                            <img src="<?= wp_get_attachment_url($estrategia['background']) ?>" alt="<?= get_post_meta($estrategia['background'], '_wp_attachment_image_alt', TRUE) ?>">
                        </div>

                        <div class="">
                            <h2 class="modal--header-title"><?= $estrategia['modal']['title'] ?></h2>
                        </div>
                    </div>

                    <div class="modal--content">
                        <div class="what">
                            <?= $estrategia['modal']['what'] ?>
                        </div>

                        <?php if (!empty($estrategia['modal']['goals'])) : ?>
                            <div class="goals">
                                <div class="goals--title">
                                    Objetivos
                                </div>

                                <div class="goals--content">
                                    <?= $estrategia['modal']['goals'] ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($estrategia['modal']['status'])) : ?>
                            <div class="status">
                                <div class="status--title">
                                    O que já vem sendo feito
                                </div>

                                <div class="status--content">
                                    <?= $estrategia['modal']['status'] ?>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if (!empty($estrategia['modal']['adicional_links'])) : ?>
                            <div class="links">
                                <div class="links--title">
                                    Para saber mais
                                </div>

                                <div class="links--content">
                                    <?= $estrategia['modal']['adicional_links'] ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>

            </div>
    <?php }
    endforeach; ?>

    <section class="estrategia-ods column large-10 large-centered">
        <?php foreach ($instance['estrategias'] as $index => $estrategia) : ?>
            <?php
            if ($estrategia['use_modal']) {
                echo '<a href="#estrategia-' . $index . '" data-modal-id="estrategia-' . $index . '" rel="modal:open">';
            }
            ?>
            <div class="estrategia">
                <img src="<?= wp_get_attachment_url($estrategia['background']) ?>" alt="<?= get_post_meta($estrategia['background'], '_wp_attachment_image_alt', TRUE) ?>">
            </div>
            <?php
            if ($estrategia['use_modal']) {
                echo '</a>';
            } ?>
        <?php endforeach; ?>
    </section>
</div>