<?php
/*
  Widget Name: Estratégia ODS
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class EstrategiaODS extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'estrategias' => array(
                'type' => 'repeater',
                'label' => __( 'Itens' , 'ods'),
                'fields' => array(
                    'background' => array(
                        'type' => 'media',
                        'label' => __('Background', 'ods' ),
                        'library' => 'image',
                    ),
                    
                    'use_modal' => array(
                        'type' => 'checkbox',
                        'label' => __('Utilizar modal para exibição?', 'ods' ),
                        'default' => false,
                    ),

                    'modal' => array(
                        'type' => 'section',
                        'label' => __( 'Conteúdo do modal' , 'ods' ),
                        'hide' => true,
                        'fields' => array(
                            'title' => array(
                                'type' => 'text',
                                'label' => __('Título', 'ods' ),
                            ),

                            'what' => array(
                                'type' => 'tinymce',
                                'label' => __( 'Do que se trata?', 'ods' ),
                                'rows' => 10,
                                'default_editor' => 'html',
                                'button_filters' => array(
                                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                                ),
                            ),

                            'goals' => array(
                                'type' => 'tinymce',
                                'label' => __( 'Objetivos', 'ods' ),
                                'rows' => 10,
                                'default_editor' => 'tinymce',
                                'button_filters' => array(
                                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                                ),
                            ),

                            'status' => array(
                                'type' => 'tinymce',
                                'label' => __( 'O que já tem sido feito', 'ods' ),
                                'rows' => 10,
                                'default_editor' => 'tinymce',
                                'button_filters' => array(
                                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                                ),
                            ),

                            'adicional_links' => array(
                                'type' => 'tinymce',
                                'label' => __( 'Links adicionais', 'ods' ),
                                'rows' => 10,
                                'default_editor' => 'tinymce',
                                'button_filters' => array(
                                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                                ),
                            ),
                        )
                    )
                )
            )
        ];

        parent::__construct('estrategia', 'Estratégia ODS ', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('estrategia', __FILE__, 'widgets\EstrategiaODS');
