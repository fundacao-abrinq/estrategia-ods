<section class="our-public">
    <div class="row">
        <div class="column large-4 medium-8 small-12">
            <div class="our-public--title">
                <?= $instance['title'] ?>
            </div>

            <div class="our-public--subtitle">
                <?= $instance['subtitle'] ?>
            </div>

            <div class="our-public--description">
                <?= $instance['description'] ?>
            </div>
            <?php $new_tab = $instance['new_tab'] ? 'target="_blank"' : ''; ?>
            <div class="our-public--buttons">
                <?php if (!empty($instance['more'])) : ?>
                    <a href="<?= $instance['more_link'] ?>" <?= $new_tab ?> class="more">
                        <?= $instance['more'] ?>
                    </a>
                <?php endif; ?>

                <?php if (!empty($instance['join_us'])) : ?>
                    <a href="<?= $instance['join_us_link'] ?>" <?= $new_tab ?> class="join-us">
                        <?= $instance['join_us'] ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>

        <div class="column medium-8 small-12">
            <div class="itens">
                <?php foreach ($instance['blocos'] as $index => $bloco) : ?>
                    <div class="item">
                        <div class="pre-content" style="background: linear-gradient(180deg, <?= $bloco['base_color'] ?> -9%, rgba(255,255,255,0) 100%);">
                            <div class="item--title">
                                <?= $bloco['title'] ?>
                            </div>
                            <div class="item--icon">
                                <img src="<?= wp_get_attachment_url($bloco['icon']) ?>" alt="<?= get_post_meta($bloco['icon'], '_wp_attachment_image_alt', TRUE) ?>">
                            </div>

                            <button class="expand-content">
                                <i class="fas fa-plus-circle"></i>
                            </button>
                        </div>
                        <div class="mobile-title item--title">
                            <?= $bloco['title'] ?>
                        </div>
                        <div class="item--content">
                            <?= $bloco['description'] ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>