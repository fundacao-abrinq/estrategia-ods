<?php
/*
  Widget Name: Nosso público
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class OurPublic extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'title' => array(
                'type' => 'text',
                'label' => __('Título', 'ods' ),
            ),

            'subtitle' => array(
                'type' => 'textarea',
                'label' => __('Subtitulo', 'ods' ),
            ),

            'description' => array(
                'type' => 'tinymce',
                'label' => __( 'Informações', 'ods' ),
                'rows' => 10,
                'default_editor' => 'html',
                'button_filters' => array(
                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                ),
            ),

            'more' => array(
                'type' => 'text',
                'label' => __('Texto do botão "mais"', 'ods' ),
            ),
            
            'more_link' => array(
                'type' => 'text',
                'label' => __('Link do botão "mais"', 'ods' ),
            ),

            'join_us' => array(
                'type' => 'text',
                'label' => __('Texto do botão "faça parte"', 'ods' ),
            ),
            
            'join_us_link' => array(
                'type' => 'text',
                'label' => __('Link do botão "faça parte"', 'ods' ),
            ),

            'new_tab' => array(
                'type' => 'checkbox',
                'label' => __('Abrir links em nova aba', 'ods' ),
            ),




            'blocos' => array(
                'type' => 'repeater',
                'label' => __( 'Blocos' , 'ods'),
                'fields' => array(
                    'title' => array(
                        'type' => 'text',
                        'label' => __('Título', 'ods' ),
                    ),

                    'base_color' => array(
                        'type' => 'color',
                        'label' => __( 'Cor base do degrade', 'ods' ),
                        'default' => '#DE1768'
                    ),

                    'icon' => array(
                        'type' => 'media',
                        'label' => __('Icone', 'ods' ),
                        'library' => 'image',
                    ),

                    'description' => array(
                        'type' => 'tinymce',
                        'label' => __( 'Conteúdo', 'ods' ),
                        'rows' => 10,
                        'default_editor' => 'html',
                        'button_filters' => array(
                            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                        ),
                    ),


                )
            )
            
        ];

        parent::__construct('our-public', 'Nosso público', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('our-public', __FILE__, 'widgets\OurPublic');
