<div class="posts-sidebar">
    <div class="posts-sidebar--title">
        <?= $instance['title'] ?>
    </div>

    <div class="posts-sidebar--content">
        <div class="posts-list horizontal">

            <?php
            $tab_query = new WP_Query($query_args);

            if ($tab_query->have_posts()) :
                while ($tab_query->have_posts()) : ?>
                    <?php $tab_query->the_post(); ?>
                    <?php

                    global $post;
                    guaraci\template_part('card', [
                        'post' => $post,
                        'horizontal' => true,
                        'show_category' => false,
                        'show_image' => true,
                        'show_excerpt' => false,
                        'show_author' => false,
                        'show_date' => true,
                    ]);

                    ?>
                <?php endwhile; ?>
            <?php else : ?>
                <div class="nothing-found">Não achamos nada aqui ;(</div>

            <?php endif; ?>
        </div>

        <?php if (!empty($instance['more'])) : ?>
            <div class="more">
                <a href="<?= $instance['more'] ?>">Ver mais</a>
            </div>
        <?php endif; ?>

    </div>
</div>