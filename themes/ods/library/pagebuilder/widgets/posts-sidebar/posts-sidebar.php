<?php

/*
  Widget Name: Listagem de posts formato sidebar	
  Description: Adiciona uma listagem de posts
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class PostsSidebar extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'title' => array(
                'type' => 'text',
                'label' => __('Título', 'ods'),
            ),

            'content' => [
                'type' => 'posts',
                'label' => 'Conteúdo'
            ],

            'more' => array(
                'type' => 'text',
                'label' => __('Link ver mais', 'ods'),
                'description' => 'Deixe vazio para não utilizar'
            )
        ];

        parent::__construct('posts-sidebar', "Listagem de posts formato sidebar", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona uma listagem de posts.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

    function get_template_variables($instance, $args) {
        $posts_query_args = siteorigin_widget_post_selector_process_query($instance['content']);

        return ['query_args' => $posts_query_args];
    }
}

Siteorigin_widget_register('posts-sidebar', __FILE__, 'widgets\PostsSidebar');
