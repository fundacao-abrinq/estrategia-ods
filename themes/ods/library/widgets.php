<?php 

function adicional_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Coluna 1 - Footer', 'ods' ),
        'id' => 'footer-area-1',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-title">',
	'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Coluna 2 - Footer', 'ods' ),
        'id' => 'footer-area-2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-title">',
	'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Coluna 3 - Footer ', 'ods' ),
        'id' => 'footer-area-3',
        'description' => __( '', 'ods' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-title">',
	'after_title'   => '</h2>',
    ) );


    register_sidebar( array(
        'name' => __( 'Coluna 4 - Footer ', 'ods' ),
        'id' => 'footer-area-4',
        'description' => __( '', 'ods' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="footer-title">',
    'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Coluna 5 - Footer ', 'ods' ),
        'id' => 'footer-area-5',
        'description' => __( '', 'ods' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="footer-title">',
    'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Cofinanciamento ', 'ods' ),
        'id' => 'footer-area-founding',
        'description' => __( '', 'ods' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="footer-title">',
    'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Comitê ', 'ods' ),
        'id' => 'footer-area-comite',
        'description' => __( '', 'ods' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="footer-title">',
    'after_title'   => '</h2>',
    ) );

  
}


add_action( 'widgets_init', 'adicional_widgets_init' );


?>