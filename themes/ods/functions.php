<?php
namespace ods;

require __DIR__ . '/library/settings.php';
require __DIR__ . '/library/images.php';
require __DIR__ . '/library/menus.php';
require __DIR__ . '/library/widgets.php';
require __DIR__ . '/library/assets.php';
require __DIR__ . '/library/pagebuilder/index.php';
// descomentar se for um multisite e for utilizar o plugin de sincronização de posts
// require __DIR__ . '/library/mssync.php';

add_theme_support( 'align-wide' );
add_theme_support( 'custom-logo' );
add_theme_support( 'post-thumbnails' );

// descomentar para importar conteúdo
// add_filter( 'force_filtered_html_on_import' , '__return_false' );

// removemos a barra de admin para não cachear no cache de borda 
add_filter( 'show_admin_bar', '__return_false' );

// usamos uma taxonomia autor criada com o plugin Pods
add_action( 'init',  function () {
	remove_post_type_support( 'post', 'author' );
});

// filtro para adicionar mais classes de título
add_filter('widgets\\SectionTitle:css_classes', function($css_classes){
	$css_classes['title-ods'] = __('Título ods', 'ods');
	return $css_classes;
});

function get_archive_title () {
	$s = isset($_GET['s']) ? trim($_GET['s']) : '';

	if($s){
		if (is_tag()) {
			$title = sprintf(__('Busca por "%s" na tag "%s"', 'ods'), $s, single_tag_title('',false));
		} elseif (is_category()) {
			$title = sprintf(__('Busca por "%s" na categoria "%s"', 'ods'), $s, single_cat_title('',false));
		} elseif (is_tax()) {
			$title = sprintf(__('Busca por "%s" em "%s"', 'ods'), $s, single_term_title('',false));
		} else {
			if(is_archive()){
				$title = sprintf(__('Busca por "%s" em "%s"', 'ods'), $s, post_type_archive_title());
			} else {
				$title = sprintf(__('Resultado da busca por "%s"', 'ods'), $s);
			}
		}
	} else {
		if (is_tag()) {
			$title = single_tag_title('Tag: ',false);
		} elseif (is_category()) {
			$title = single_cat_title('Categoria: ',false);
		} elseif (is_tax()) {
			$title = single_term_title('',false);
		} else {
			$title = post_type_archive_title('', false);
		}
	}

	return $title;
}

add_filter('pre_get_posts', 'ods\_search_pre_get_posts', 1);

function _search_pre_get_posts($query) {
	if (is_admin() || !$query->is_main_query()) {
		return $query;
	}

	if ($query->is_search() && $query->is_main_query()) {
		//$query->set('post_type', [$query->query['post_type']]);
        
		// Date filter
		if (isset($_GET['daterange'])) {
			$date_range = explode(' - ', $_GET['daterange'], 2);
			if (sizeof($date_range) == 2) {
				$from_date = date_parse($date_range[0]);
                $to_date   = date_parse($date_range[1]);
				$after  = null;
				$before = null;

				if ($from_date && checkdate($from_date['month'], $from_date['day'], $from_date['year'])) {
					$after = array(
						'year'  => $from_date['year'],
						'month' => $from_date['month'],
						'day'   => $from_date['day'],
					);
				}
				// Same for the "to" date.
				if ($to_date && checkdate($to_date['month'], $to_date['day'], $to_date['year'])) {
					$before = array(
						'year'  => $to_date['year'],
						'month' => $to_date['month'],
						'day'   => $to_date['day'],
					);
				}

				$date_query = array();
				if ($after) {
					$date_query['after'] = $after;
				}
				if ($before) {
					$date_query['before'] = $before;
				}
				if ($after || $before) {
					$date_query['inclusive'] = true;
				}

				if (!empty($date_query)) {
					$query->set('date_query', $date_query);
				}
			}
		}

        // order
        $query->set('order', 'DESC');
        $query->set('orderby', 'date');

        if (isset($_GET['order'])) {  
            if ($_GET['order'] == 'title') {
                $query->set('order', 'ASC');
                $query->set('orderby', 'title');
            } elseif ($_GET['order'] == 'date') {
                $query->set('order', 'DESC');
                $query->set('orderby', 'date');
            }	
        }

        // categoria e midia
        if (isset($_GET['midia_filter']) && !empty($_GET['midia_filter']) && isset($_GET['category_filter']) && !empty($_GET['category_filter']) ) {            
            $tax_query = [
                'relation' => 'AND',
                [
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => $_GET['category_filter']
                ],
                [
                    'taxonomy' => 'midia',
                    'field'    => 'slug',
                    'terms'    => $_GET['midia_filter']
                ]
            ];

            $query->set('tax_query', $tax_query);

        } else {
            if (isset($_GET['category_filter']) && !empty($_GET['category_filter'])) {
                $query->set('category_name', $_GET['category_filter']);
            }
            
            if (isset($_GET['midia_filter']) && !empty($_GET['midia_filter'])) {
                $query->set('tax_query', [
                    array(
                        'taxonomy' => 'midia',
                        'field'    => 'slug',
                        'terms'    => $_GET['midia_filter']
                    ),
                ]);

            }
        }
        //var_dump($query);

    }
    
    return $query;
    
}


function loadmore_ajax_handler(){
	// prepare our arguments for the query
    $args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
    $args['post_status'] = 'publish';
    
	// it is always better to use WP_Query but not here
    query_posts( $args );

	if( have_posts() ) :
        
		// run the loop
        while( have_posts() ):
            the_post();
            global $post;
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			\guaraci\template_part('card', [
                'post' => $post,
                'horizontal' => true,
                'use_hat' => false,
                'show_category' => false,
                'show_image' => true,
                'show_excerpt ' => true,
                'show_author' => false,
                'show_date' => true,
            ]);
			// for the test purposes comment the line above and uncomment the below one
			// the_title();
 
 		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
  
add_action('wp_ajax_loadmore', 'ods\\loadmore_ajax_handler');
add_action('wp_ajax_nopriv_loadmore', 'ods\\loadmore_ajax_handler');


/**
 * Retorna a quantidade de posts de acordo com os parâmetros enviados
 *
 * @param array     $post_type
 * @param string    $taxonomy
 * @param array     $terms
 * @param array     $status
 * @return int
 */
function get_count_posts($post_type = ['post'], $taxonomy = '', $terms = [], $status = ['publish']) {
    
    $args = [
        'post_type'      => $post_type,
        'post_status'    => $status, 
        'posts_per_page' => -1,
        'tax_query' => [
            [
                'taxonomy' => $taxonomy,
                'terms'    => $terms
            ]
        ]
    ];

    $query = new \WP_Query($args);

    return (int) $query->post_count;

}

/**
 * Melhoria nos resumos do WordPress
 *
 * @param string $content
 * @param string $limit
 * @param string $after
 * @return void
 */
function get_excerpt($content = '', $limit = '', $after = '')
{

    if ($limit) {
        $l = $limit;
    } else {
        $l = '240';
    }

    if ($content) {
        $excerpt = $content;
    } elseif(has_excerpt()) {
        return get_the_excerpt();
    } else {
        $excerpt = get_the_content();
    }

    $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $l);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));

    if ($after) {
        $a = $after;
    } else {
        $a = '...';
    }

    if (!empty($excerpt)) {
        $excerpt = $excerpt . $a;
        return $excerpt;
    }

}
