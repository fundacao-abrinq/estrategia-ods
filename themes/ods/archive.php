<?php
get_header();
$title = '';
?>
<div class="row">
    <div class="column large-8 medium-7 small-12 list">
        <?php if (is_tag() || is_category() || is_tax() || is_archive()) : ?>
            <?php guaraci\template_part('archive-title', ['title' => ods\get_archive_title()]) ?>
        <?php else : ?>
            <?php guaraci\template_part('archive-title', ['title' => single_post_title('', false)]) ?>
        <?php endif; ?>

        <?php guaraci\template_part('posts-list', [
            'show_category' => false,
            'show_image' => true,
            'show_excerpt ' => true,
            'show_author' => false,
            'show_date' => true,
        ]); ?>
    </div>

    <div class="column large-4 medium-5 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
</div>

<?php get_footer();
