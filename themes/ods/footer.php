</div>
<?php wp_reset_postdata() ?>
<!-- <div class="page--share">
    <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
    <a class="messenger" href="http://www.facebook.com/dialog/send?app_id=346307852736540&link=<?= get_the_permalink() ?>&redirect_uri=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-facebook-messenger"></i></a>
    
    <a class="twitter" href="https://twitter.com/intent/tweet?text=<?= urlencode(get_the_title()) ?>&url=<?= get_the_permalink() ?>" target="_blank"><i class="fab fa-twitter"></i></a>
    <a class="whatsapp hide-for-large" href="whatsapp://send?text=<?= (get_the_title() . ' - ' . get_the_permalink()) ?>" target="_blank"><i class="fab fa-whatsapp"></i></a>
    <a class="whatsapp show-for-large" href="https://api.whatsapp.com/send?text=<?= (get_the_title() . ' - ' . get_the_permalink()) ?>" target="_blank"><i class="fab fa-whatsapp"></i></a>
    <a class="telegram" href="https://telegram.me/share/url?url=<?= get_the_title() . ' - ' . get_the_permalink() ?>" target="_blank"><i class="fab fa-telegram"></i></a>
    <a class="mail" href="mailto:?subject=<?= the_title() ?>&body=<?= get_the_permalink() ?>" target="_blank"><i class="far fa-envelope"></i></a>
</div> -->

<footer class="main-footer row">
    <div class="column large-12">
        <div class="footer-wrapper">
            <div class="footer-header">
                <div class="social-menu column large-3">
                    <?php ods\the_social_networks_menu(false) ?>
                </div>
                <div class="column large-6 logo">
                    <a href="/" class=" logo"><img src="<?= get_template_directory_uri() ?>/assets/images/logo.png" alt="Logotipo" height="120"></a>
                </div>
                <div class="search-form column large-3">
                    <form action="/">
                        <input type="text" name="s" id="s" placeholder="Digite aqui sua busca...">
                        <input type="submit" class="fas" value="">
                    </form>
                </div>
            </div>

            <div class="footer-content">
                <div class="footer-menus">
                    <?php for ($i = 1; $i <= 5; $i++) : ?>
                        <div class="menu-column">
                            <?php dynamic_sidebar('footer-area-' . $i); ?>
                        </div>
                    <?php endfor; ?>
                </div>

                <div class="footer-sponsored">
                    <div class="column large-2">
                        <h2 class="footer-title">Cofinanciamento</h2>
                        <?php dynamic_sidebar('footer-area-founding'); ?>
                    </div>
                    <div class="column large-10">
                        <h2 class="footer-title">Comitê gestor</h2>
                        <div class="comite">
                            <?php dynamic_sidebar('footer-area-comite'); ?>
                        </div>
                    </div>
                </div>

                <div class="hacklab">
                    <?php guaraci\template_part('site-by-hacklab') ?>
                </div>
                
            </div>

            
        </div>

        
    </div>
</footer>
<?php wp_footer() ?>

</body>

</html>