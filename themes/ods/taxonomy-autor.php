<?php

use guaraci\authors;

get_header();

$term = $wp_query->get_queried_object();
?>
<div class="row mt-20 pt-60">
    <div class="column large-8 medium-7 small-12">

        <div class="author-box">

            <?php
            $avatar = authors::get_avatar($term);
            if (!empty($avatar)) : ?>

            <div class="author-avatar column large-3" role="img">
                <img src="<?= $avatar ?>">
            </div><!-- /.author-avatar -->

            <div class="author-info column large-9">

            <?php else : ?>

            <div class="author-info column large-12">

                <?php endif; ?>

                <h3><strong><?= $term->name ?></strong></h3>

                <?php
                $author_area = get_term_meta($term->term_id, 'area', true);
                if (!empty($author_area)) : ?>
                    <span class="author-area"><?= $author_area ?></span>
                <?php endif; ?>

                <?php if (!empty($term->description)) : ?>
                    <div class="text-left"><?= $term->description ?></div>
                <?php endif; ?>

            </div><!-- /.author-info -->

            <div class="author-social column row">
                
                <div class="column large-3">
                    <h4>Contatos</h4>
                </div>
                <div class="icons column large-9">
                    <?php
                    $author_fb = get_term_meta($term->term_id, 'facebook', true);
                    if (!empty($author_fb)) {
                        echo '<a href="' . $author_fb . '" ><i class="fab fa-facebook-f"></i></a>';
                    }

                    $author_twitter = get_term_meta($term->term_id, 'twitter', true);
                    if (!empty($author_twitter)) {
                        echo '<a href="' . $author_twitter . '" ><i class="fab fa-twitter"></i></a>';
                    }

                    $author_email = get_term_meta($term->term_id, 'email', true);
                    if (!empty($author_email)) {
                        echo '<a href="mailto:' . $author_email . '" ><i class="far fa-envelope"></i></a>';
                    }

                    $author_medium = get_term_meta($term->term_id, 'medium', true);
                    if (!empty($author_medium)) {
                        echo '<a href="' . $author_medium . '" ><i class="fab fa-medium-m"></i></a>';
                    }
                    ?>
                </div>

            </div><!-- /.author-social -->

        </div><!-- /.author-box -->
        
        <div class="column medium-12 small-12">
            <?php guaraci\template_part('posts-list', ['title' => $title, 'hide_author' => true]); ?>
        </div>
    
    </div>

    <div class="column large-4 medium-5 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets', ['sidebar_slug' => 'autor']); ?>
    </div>
</div>

<?php get_footer();
