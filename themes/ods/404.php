<?php get_header(); ?>

<div class="error-404">
    <h1><?php _e('erro', 'ods') ?><br><strong><?php _e('404', 'ods') ?></strong></h1>
    <p><?php _e('Página não encontrada', 'ods') ?></p>
    <a href="/" class="button"> <span><?php _e('Voltar para a página inicial', 'ods') ?></span> </a>
</div>

<?php get_footer(); ?>