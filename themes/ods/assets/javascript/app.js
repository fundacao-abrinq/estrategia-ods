import "./functionalities/header";
import "./functionalities/slider";
import "./functionalities/modal";
import "./functionalities/our-public";
import "./functionalities/filtarable-posts";
import "./functionalities/posts-tabs";
import "./functionalities/post-share";
import "./functionalities/search";
