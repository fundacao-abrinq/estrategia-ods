jQuery(document).ready(function() {
    jQuery('input[name="daterange"]').daterangepicker({
        minDate: "01/01/2010",
        maxDate: new Date(),
        autoUpdateInput: false,
        locale: {
            applyLabel: "Aplicar",
            cancelLabel: "Limpar",
            "fromLabel": "De",
            "toLabel": "Até",
            "customRangeLabel": "Personalizado",
            "weekLabel": "S",
            "daysOfWeek": [
                "Dom",
                "Seg",
                "Ter",
                "Qua",
                "Qui",
                "Sex",
                "Sab"
            ],
            "monthNames": [
                "Janeiro",
                "Fevereiro",
                "Março",
                "Abril",
                "Maio",
                "Junho",
                "Julho",
                "Agosto",
                "Setembro",
                "Outubro",
                "Novembro",
                "Dezembro"
            ],
            "firstDay": 1
        }
    });

    jQuery('input[name="daterange"]').on("apply.daterangepicker", function(ev, picker) {
        jQuery(this).val(
            picker.startDate.format("DD/MM/YYYY") +
                " - " +
                picker.endDate.format("DD/MM/YYYY")
        );

        jQuery(this)
            .closest("form")
            .submit();
    });

    jQuery('input[name="daterange"]').on("cancel.daterangepicker", function(ev,picker) {
        jQuery(this).val("");
    });


    jQuery(".custom-filters--filters select").change(function () {
        jQuery(this).closest("form").submit();
    });

    jQuery(".custom-sort select").change(function () {
        jQuery(this).closest("form").submit();
    });

});
