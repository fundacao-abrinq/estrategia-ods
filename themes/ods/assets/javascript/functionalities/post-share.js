// se existe o page-share na pagina, executa as funções
if (jQuery('.page--share').length) {

    jQuery(document).ready(function () {
        onloadPageShare();
    });

    jQuery(window).on("resize", function () {
        onresizePageShare();
    });

    jQuery(window).scroll(function () {
        onscrollPageShare();
    });

};

function onloadPageShare() {
    if (jQuery(window).width() >= 1300) {
        jQuery('.page--share').css('left', jQuery('.header--content').offset().left + 'px');
    }
}

function onresizePageShare() {
    if (jQuery(window).width() >= 1300) {
        jQuery('.page--share').css('left', jQuery('.header--content').offset().left + 'px')
    } else {
        jQuery('.page--share').css('left', '0');
    }
}

function onscrollPageShare() {
    if (jQuery(document).scrollTop() > jQuery('.related-posts').position().top - 600 && jQuery(window).width() >= 1360) {
        jQuery('.page--share').css('display', 'none');
    } else if (jQuery(document).scrollTop() > jQuery('.related-posts').position().top - 900 && jQuery(window).width() < 1360) {
        jQuery('.page--share').css('display', 'none');
    } else {
        jQuery('.page--share').css('display', 'flex');
    }
}
