if (document.querySelectorAll('.slider-section').length){
    const slider = new Flickity('.slider-section', {
        cellSelector: '.carousel-cell',
        cellAlign: 'center',
        prevNextButtons: false,
        wrapAround: true,
        // arrowShape: 'M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z'
    });

    jQuery('button.arrow-prev').click(function() {
        slider.previous();
    })
    
    jQuery('button.arrow-next').click(function() {
        slider.next();
    })
}


if (document.querySelectorAll('.quotes').length){
    const slider2 = new Flickity('.quotes', {
        cellSelector: '.quote',
        cellAlign: 'center',
        prevNextButtons: false,
        wrapAround: true,
    });
    
    jQuery('.quotes button.previous').click(function() {
        slider2.previous();
    })
    
    jQuery('.quotes button.next').click(function() {
        slider2.next();
    })    
}

