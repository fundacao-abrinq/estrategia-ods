jQuery(function($){
	var canBeLoaded = true, 
	    bottomOffset = 2000;
 
    let firstLoad = true;
    let selectedCategoryCount = -1;
    const postList = $('.ajax-posts .posts-list');
    const postsContainer = postList.find('.posts-list--content');
    const initialTotal = $('.total > strong').text();
    const totalElement = $('.total > strong');
    const counterElement = $('.actual-number > strong');

    if(!postList.length) {
        return;
    }

    $(postsContainer).bind('DOMSubtreeModified', function(){
        // if(postsContainer.children().length != 0) {
            counterElement.text(postsContainer.children().length);
        // }
    });

    const data = {
        'action': 'loadmore',
        'query': ods_params.posts,
        'page' : ods_params.current_page,
    };

    $('select#sort-options').change(function() {
        const processedArgs = JSON.parse(ods_params.posts);

        if($(this).val() == 'title') {
            processedArgs['orderby'] = 'title';
            processedArgs['order'] = 'ASC';
        } else {
            processedArgs['orderby'] = 'date';
            processedArgs['order'] = 'DESC';
        }
        
        ods_params.posts = JSON.stringify(processedArgs);
        ods_params.current_page = 0;

        postsContainer.html('');

        buildPosts(ods_params, {
            'action': 'loadmore',
            'query': ods_params.posts,
            'page' : ods_params.current_page,
        }, postList, false);
    });

	$(window).on("load scroll", function(){        
		if( ($(document).scrollTop() > ( $(document).height() - bottomOffset ) && canBeLoaded == true)  || firstLoad){
            const processedArgs = JSON.parse(ods_params.posts);
            firstLoad = false;

            if(processedArgs.posts_per_page * ods_params.current_page < selectedCategoryCount || selectedCategoryCount < 0) {    
                buildPosts(ods_params, {
                    'action': 'loadmore',
                    'query': ods_params.posts,
                    'page' : ods_params.current_page,
                }, postList);
            }

		}
    });
    
    $('.ajax-posts .filtro-posts .categories > a').click(function() {
        const processedArgs = JSON.parse(ods_params.posts);
        const categoryId    = parseInt($(this).attr('data-id'));
        const itensCount    = parseInt($(this).attr('data-count'));
        const taxonomy      = $(this).attr('data-taxonomy');
        
        selectedCategoryCount = categoryId > 0? itensCount : -1;
        
        if (taxonomy != 'category') {
            var args = [{
                'taxonomy': taxonomy,
                'terms'   : categoryId > 0? categoryId : "all"
            }];
            processedArgs['tax_query'] = args;
        } else {
            processedArgs['category__in'] = categoryId > 0? categoryId : "";
        }
        
        ods_params.posts = JSON.stringify(processedArgs);
        ods_params.current_page = 0;

        totalElement.text(selectedCategoryCount >= 0? selectedCategoryCount : initialTotal);

        $('.ajax-posts .filtro-posts .categories > a').removeClass('active');
        $(this).toggleClass('active');

        postsContainer.html('');

        // console.log(ods_params);
        buildPosts(ods_params, {
            'action': 'loadmore',
            'query': ods_params.posts,
            'page' : ods_params.current_page,
        }, postList, false);
    });


    function buildPosts(params, data, postList, incrementContent = true) {
        // console.log(params);

        const postsContainer = postList.find('.posts-list--content');

        jQuery.ajax({
            url : params.ajaxurl,
            data: data,
            type: 'POST',
            beforeSend: function( xhr ){
                if(params.current_page + 1 >= params.max_page ) {
                    postList.find('.loading-posts').hide();
                }
                
                canBeLoaded = false; 
                postList.find('.loading-posts').addClass('active');
            },
            success: function(data){
                if( data ) {
                    // console.log(data)

                    if(incrementContent) {
                        postsContainer.html(postsContainer.html() + data );
                    } else {
                        // console.log(data)
                        postsContainer.html( data );
                    }
                    
                    document.location.hash = 'page:' + params.current_page;
                    params.current_page++;

                    canBeLoaded = true;
                    
                    postList.find('.loading-posts').removeClass('active');
                }
            },
            
        });
    }
});

