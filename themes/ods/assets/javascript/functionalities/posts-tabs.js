window.addEventListener('DOMContentLoaded',function () {
    document.querySelectorAll('.posts-tabs').forEach(function(element, index) {
        let currentActive = 0;

        element.querySelectorAll('.posts-tabs--selector-tabs button').forEach(function(button) {
            button.onclick = function() {
                const targetTab = this.getAttribute('target-tab');
                // console.log(currentActive, targetTab);

                if(currentActive == targetTab) return;
                //element.querySelector('.posts-tabs--tabs .tab[tab="' + targetTab + '"]').classList.toggle('active');

                element.querySelectorAll('.posts-tabs--tabs .tab').forEach(function(tab) {
                    if(tab.getAttribute('tab') == targetTab) {
                        element.querySelector('.posts-tabs--selector-tabs button[target-tab="' + currentActive + '"]').classList.remove('active');

                        tab.classList.toggle('active');
                        button.classList.toggle('active');
                        currentActive = targetTab;
                    } else {
                        tab.classList.remove('active');
                    }
                })
            }
        })
    });
});