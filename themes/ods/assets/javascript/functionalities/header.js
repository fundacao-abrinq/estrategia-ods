jQuery.fn.toggleFocus = function() {
    if (this.is(":focus")) {
        this.blur();
    } else {
        this.focus();
    }
};

jQuery(document).ready(function() {
    jQuery(".search-form button").click(function() {
        if (jQuery(".search-form input").val().length > 0) {
            jQuery(".search-form form").submit();
        }
    
        jQuery("header .search-form").toggleClass("active");
        jQuery("header .search-form input").toggleFocus();
    });
    
    jQuery('.menu-btn').click(function() {
        jQuery('.menu-btn').toggleClass('active');
        jQuery('.menu-wrapper').toggleClass('active');
        jQuery('header.header').toggleClass('active');
    });

    const header = jQuery('header.header');
    let didScroll;
    let lastScrollTop = 0;
    let delta = 2;
    let navbarHeight = header.outerHeight();

    jQuery(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var currentScrollTop = jQuery(document).scrollTop();

        if(Math.abs(lastScrollTop - currentScrollTop) <= delta)
            return;
    
        if (currentScrollTop > lastScrollTop && currentScrollTop > navbarHeight){
            header.removeClass('nav-down').addClass('nav-up');
        } else {
            if(currentScrollTop + jQuery(window).height() < jQuery(document).height()) {
                header.removeClass('nav-up').addClass('nav-down');
            }
        }
    
        lastScrollTop = currentScrollTop;
    }

});
