jQuery(document).ready(function() {
    if (jQuery(".categorical-posts .posts-list").length) {
        const element = document.querySelector(
            ".categorical-posts .posts-list"
        );
        const chosenCategories = [];

        document
            .querySelectorAll(".filterable-card")
            .forEach(function(element) {
                const elementCategories = JSON.parse(
                    element.getAttribute("data-groups")
                );

                elementCategories.forEach(function(catItem) {
                    if (!chosenCategories.includes(catItem)) {
                        chosenCategories.push(catItem);
                    }
                });
            });

        chosenCategories.forEach(function(item) {
            const catFilterLink = document.createElement("a");
            catFilterLink.setAttribute("data-filter", item);
            catFilterLink.innerHTML = item;
            document
                .querySelector(".filtro-posts .categories")
                .appendChild(catFilterLink);
        });

        //console.log(chosenCategories);

        let shuffleInstance = new Shuffle(element, {
            itemSelector: ".filterable-card",
            useTransforms: true
        });

        shuffleInstance.enable();

        let categoryCounter = {};
        if (
            jQuery(element)
                .closest(".categorical-posts")
                .attr("remove-repeated") == "true"
        ) {
            shuffleInstance.filter(function(element) {
                const data = JSON.parse(jQuery(element).attr("data-groups"))[0];

                if (!(data in categoryCounter)) {
                    categoryCounter[data] = 1;
                } else {
                    categoryCounter[data] += 1;
                }
                return categoryCounter[data] <= 1;
            });
        }

        let current_selected = jQuery(".filtro-posts .categories a.active");
        //jQuery('.read-more-button').attr('href', current_selected.attr('data-link'));

        jQuery(".filtro-posts .categories a").click(function(e) {
            e.preventDefault();

            current_selected.removeClass("active");
            current_selected = jQuery(this);
            current_selected.addClass("active");

            //jQuery('.read-more-button').attr('href', current_selected.attr('data-link'));

            const selectedFilter = jQuery(this).attr("data-filter");

            if (selectedFilter.length == 0) {
                categoryCounter = {};
                // console.log(
                //     jQuery(this)
                //         .closest(".filtro-posts")
                //         .attr("all-filter")
                // );

                if (
                    jQuery(element)
                        .closest(".categorical-posts")
                        .attr("remove-repeated") == "true"
                ) {
                    shuffleInstance.filter(function(element) {
                        const data = JSON.parse(
                            jQuery(element).attr("data-groups")
                        )[0];

                        if (!(data in categoryCounter)) {
                            categoryCounter[data] = 1;
                        } else {
                            categoryCounter[data] += 1;
                        }
                        return categoryCounter[data] <= 1;
                    });
                } else {
                    shuffleInstance.filter(_ => true);
                }
            } else {
                const itensId = [];
                shuffleInstance.filter(function(element) {
                    const elementCategories = JSON.parse(
                        jQuery(element).attr("data-groups")
                    );
                    //console.log(elementCategories, selectedFilter);
                    itensId.push(jQuery(element).attr("post-id"));

                    return (
                        elementCategories.includes(selectedFilter) &&
                        itensId.filter(function(itemId) {
                            return itemId === jQuery(element).attr("post-id");
                        }).length <= 1
                    );
                });
            }
        });
    }
});
