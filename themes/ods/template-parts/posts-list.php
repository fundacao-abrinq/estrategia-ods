<?php
$show_category  = isset($show_category) ? $show_category : true;
$show_image     = isset($show_image) ? $show_image : true;
$show_excerpt   = isset($show_excerpt) ? $show_excerpt : true;
$use_hat        = isset($use_hat) ? $use_hat : false;
$show_author    = isset($show_author) ? $show_author : true;
$show_date      = isset($show_date) ? $show_date : true;
$horizontal     = isset($horizontal) ? $horizontal : true;
$card           = isset($card) ? $card : 'card';
$use_pagination = isset($use_pagination) ? $use_pagination : true;
?>
<div class="posts-list <?= $horizontal ? 'horizontal' : 'list' ?>">
    <?php
    while (have_posts()) : the_post();
        global $post;
        guaraci\template_part($card, [
            'post'          => $post,
            'horizontal'    => $horizontal,
            'show_category' => $show_category,
            'show_image'    => $show_image,
            'show_excerpt'  => $show_excerpt,
            'show_author'   => $show_author,
            'show_date'     => $show_date,
            'use_hat'       => $use_hat
        ]);
    endwhile ?>
</div>

<?php if ($use_pagination) : 
    global $wp_query;
    $request = basename($_SERVER['REQUEST_URI']);
    $request = substr($request, 1);

    if($wp_query->found_posts >= $wp_query->query_vars['posts_per_page']) : ?>

        <div class="pagination-numbers">
            <a class="first prev page-numbers" href="<?php echo home_url('/page/1/' . $request) ?>"><<</a>
            <?php guaraci\pagination_links(); ?>
            <a class="last next page-numbers" href="<?php echo home_url('/page/' . $wp_query->max_num_pages . $request) ?>">>></a>
        </div>
        
    <?php endif;
endif;