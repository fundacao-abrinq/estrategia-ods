<?php
$taxonomy = 'category';
if ('multimedia' == get_post_type()) {
    $taxonomy = 'midia';
}  
?>

<div class="filtro-posts">
    <div class="categories">
        <a href="#" class="active" data-id="-1" data-taxonomy="<?= $taxonomy ?>">
            Todas
        </a>
        <?php
        $terms = get_terms($taxonomy);
        foreach ($terms as $term) :
            $count = ods\get_count_posts(get_post_type(), $taxonomy, $term->term_id);
            if($count >= 1) : ?>
            <a data-id="<?= $term->term_id ?>" data-count="<?= $count ?>" data-taxonomy="<?= $taxonomy ?>">
                <?= $term->name ?>
            </a>
        <?php endif;
        endforeach;
        ?>
    </div>
</div>

<div class="content-options">
    <div class="posts-quantity">
        Exibindo
        <span class="actual-number"> <strong> 0 </strong>
        de
        <span class="total">
            <strong> 
            <?php 
                global $wp_query;
                echo $wp_query->found_posts;
            ?>
            </strong>
        </span>
        conteúdos:
    </div>

    <label for="sort-options" class="filter-options">
        Ordenando por:
        <select name="sort" id="sort-options">
            <option value="date">Data</option>
            <option value="title">Título</option>
        </select>
    </label>
</div>

<div class="posts-list horizontal">
    <div class="posts-list--content">

    </div>
    <div class="loading-posts">
        <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>

        <span><?= __('Carregando mais conteúdo...', 'ods') ?></span>
    </div>
</div>