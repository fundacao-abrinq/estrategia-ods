<div class="featured-header">
    <div class="row">
        <div class="column large-12">
            <h1>
                <?= isset($title) ? $title : '' ?>
            </h1>

            <div class="content">
                <p>
                    <?= isset($content) ? $content : '' ?>
                </p>
            </div>

        </div>
    </div>
</div>