<h1>
    <?php _e("Resultado da busca", "ods") ?>
</h1>
<form action="" class="advanced-search">
    <div class="input-search">
        <input type="text" name="s" placeholder="Digite aqui sua busca..." value="<?= isset($_GET['s']) ? $_GET['s'] : '' ?>">
        <button type="submit">
            <i class="fas fa-search"></i>
        </button>
    </div>

    <div class="custom-filters">
        <div class="custom-filters--title">
            <?php _e("Filtre a busca", "ods") ?>
        </div>

        <div class="custom-filters--filters">
            <div class="filter">
                <label for="daterange">Datas</label>
                <input type="text" readonly value="<?= isset($_GET['daterange']) || !empty($_GET['daterange']) ? $_GET['daterange'] : '' ?>" replace-empty="<?= !isset($_GET['daterange']) || empty($_GET['daterange']) ? 'true' : 'false' ?>" autocomplete="off" placeholder="Todas" name="daterange">
            </div>

            <?php
            $categories = get_categories();
            //var_dump($categories);
            ?>

            <div class="filter">
                <label for="category_filter">
                    Categorias
                </label>
                <select name="category_filter" id="category">
                    <option value=""> Todas </option>
                    <?php
                    foreach ($categories as $term) : ?>
                        <option value="<?= $term->slug ?>" <?= isset($_GET['category_filter']) && $_GET['category_filter'] == $term->slug ? 'selected' : '' ?>> <?= $term->name ?> </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <?php $midia = get_terms(['taxonomy' => 'midia']); ?>
            <div class="filter">
                <label for="midia">
                    Mídias
                </label>
                <select name="midia_filter" id="midia">
                    <option value=""> Todas </option>
                    <?php
                    foreach ($midia as $term) : ?>
                        <option value="<?= $term->slug ?>" <?= isset($_GET['midia_filter']) && $_GET['midia_filter'] == $term->slug ? 'selected' : '' ?>> <?= $term->name ?> </option>
                    <?php endforeach; ?>
                </select>
            </div>

        </div>
    </div>

    <div class="custom-sort">

        <?php

        global $wp_query;

        $paged = ($wp_query->query_vars['paged']) ? $wp_query->query_vars['paged'] : 1;
        if ($paged == $wp_query->max_num_pages) {
            $show = $wp_query->found_posts;
        } else {
            $show = $paged * $wp_query->query_vars['posts_per_page'];
        } ?>

        <?php if ($wp_query->found_posts >= 1) : ?>
            <span class="quantity">Exibindo <b><?= $show ?></b> de <?= $wp_query->found_posts ?> conteúdos: </span>
        <?php else : ?>
            <span class="quantity no-results">Nenhum conteúdo para exibir!</span>
        <?php endif; ?>

        <div class="filter">
            <label for="order">
                <?= __('Ordenar por:', 'ods'); ?>
                <select name="order" id="order">
                    <option value="date" <?= isset($_GET['order']) && $_GET['order'] == 'date' ? 'selected' : '' ?>><?= __('Data', 'ods'); ?></option>
                    <option value="title" <?= isset($_GET['order']) && $_GET['order'] == 'title' ? 'selected' : '' ?>><?= __('Título', 'ods'); ?></option>
                </select>
            </label>
        </div>

    </div><!-- /.custom-sort -->

</form>