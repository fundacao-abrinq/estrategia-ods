<?php 
if(!get_option( 'related_posts__use' )){
    return;
} 
if(empty($post_id)){
    $post_id = get_the_ID();
}
?>
<div class="post-content--section-title">
    <?php _e('Conteúdos relacionados', 'ods'); ?>
</div>
<div class="post-content--related-posts">
    <?php
    $post_type = [get_post_type($post_id)];
    $related_posts_query = guaraci\related_posts::get_posts($post_id, 4, $post_type);
    $related_posts = [];

    if ( $related_posts_query->have_posts() ) {
        while( $related_posts_query->have_posts() ) {
            $related_posts_query->the_post();
            guaraci\template_part('card', ['show_category' => false, 'show_image' => true, 'show_excerpt' => false]);
        }
    }
    ?>
    <?php wp_reset_postdata(); ?>
</div>

