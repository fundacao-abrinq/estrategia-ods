<?php

use guaraci\authors;
use guaraci\images;

$show_image      = isset($show_image) ? $show_image : true;
$show_excerpt    = isset($show_excerpt) ? $show_excerpt : true;
$show_category   = isset($show_category) ? $show_category : true;
$show_author     = isset($show_author) ? $show_author : true;
$show_date       = isset($show_date) ? $show_date : true;
$use_hat         = isset($use_hat) ? $use_hat : false;
$horizontal      = isset($horizontal) ? $horizontal : false;
$author_taxonomy = isset($author_taxonomy) ? $author_taxonomy : 'autor';
$authors         = authors::get($author_taxonomy, false);
$category_str    = get_the_category_list(', ');
$image_tag       = images::tag('card-large', 'card--image');
$has_image       = $show_image && $image_tag;
$uses_excerpt    = post_type_supports(get_post_type(), 'excerpt');
$classes         = isset($classes) ? $classes : "";
$attributes      = isset($attributes) ? $attributes : "";
?>

<div class="card <?= $classes ?>" <?= $attributes ?> post-id="<?= get_the_ID() ?>">
    <?php if ($has_image) : ?>
        <a tabindex="-1" href="<?= get_the_permalink() ?>" class="card--image-wrapper">
            <?= $image_tag ?>
        </a>
    <?php endif; ?>

    <div class="card--content">

        <h4 class="card--title">
            <a href="<?= get_the_permalink() ?>"><?php the_title() ?></a>
        </h4>

        <?php if ($show_category) : ?>

            <?php $post_categories = wp_get_post_categories(get_the_ID()); ?>

            <?php if ($post_categories) : ?>

                <span class="categories-list">
                    <?php foreach ($post_categories as $category_id) {
                        $category = get_category($category_id); ?>
                        <div class="categories-list--category category-<?= $category->slug ?>">
                            <a href="<?= esc_url(get_term_link($category, 'category')) ?>"><?= $category->name ?></a>
                        </div>
                    <?php } ?>
                </span><!-- /.categories-list -->
                <span class="separator"> | </span><!-- /.separator -->

            <?php endif; ?>

        <?php endif; ?>

        <?php if ($show_date && !$show_author) : ?>
            <span class="date">
                <?= 'Publicado em ' . get_the_date() ?>
            </span>
        <?php endif; ?>

        <?php if ($show_author) : ?>
            <?php if (!empty($authors)) : ?>
                <span class="span-author">
                    <?php
                    $authors = wp_get_post_terms($post->ID, $author_taxonomy);
                    if (!empty($authors)) {
                        echo implode(' ', array_map(function ($author) {
                            $url = get_term_link($author);
                            //var_dump(get_term_meta($author->term_id, 'avatar', true));
                            if (!empty(get_term_meta($author->term_id, 'avatar', true))) {
                                echo wp_get_attachment_image(get_term_meta($author->term_id, 'avatar', true)['ID'], 'thumbnail', false, ['class' => 'author-avatar']);
                            } ?>

                            <div class="modal modal-author" id="author-<?= $author->term_id ?>">
                                <div class="modal--wrapper">
                                    <div class="modal--header">
                                        <div class="featured-image">
                                            <?php
                                            if (!empty(get_term_meta($author->term_id, 'avatar', true))) {
                                                echo wp_get_attachment_image(get_term_meta($author->term_id, 'avatar', true)['ID']);
                                            }
                                            ?>
                                        </div>

                                        <div class="introduction">
                                            <h2 class="modal--header-title"><?= $author->name ?></h2>
                                            <span>
                                                <?php
                                                $author_area = get_term_meta($author->term_id, 'area', true);
                                                if (!empty($author_area)) {
                                                    echo $author_area;
                                                } ?>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="modal--content">
                                        <div class="spacer"></div>
                                        <div class="author-description">
                                            <?= $author->description ?>
                                        </div>

                                        <div class="spacer contact">
                                            Contatos
                                        </div>

                                        <div class="icons">
                                            <?php
                                            $author_fb = get_term_meta($author->term_id, 'facebook', true);
                                            if (!empty($author_fb)) {
                                                echo '<a href="' . $author_fb . '" ><i class="fab fa-facebook-f"></i></a>';
                                            }

                                            $author_twitter = get_term_meta($author->term_id, 'twitter', true);
                                            if (!empty($author_twitter)) {
                                                echo '<a href="' . $author_twitter . '" ><i class="fab fa-twitter"></i></a>';
                                            }

                                            $author_email = get_term_meta($author->term_id, 'email', true);
                                            if (!empty($author_email)) {
                                                echo '<a href="mailto:' . $author_email . '" ><i class="far fa-envelope"></i></a>';
                                            }

                                            $author_medium = get_term_meta($author->term_id, 'medium', true);
                                            if (!empty($author_medium)) {
                                                echo '<a href="' . $author_medium . '" ><i class="fab fa-medium-m"></i></a>';
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>

                            </div>

                    <?php
                            return "por <a href=\"#author-" . $author->term_id . "\" rel=\"modal:open\">" . $author->name . "</a>";
                        }, $authors));
                    } ?>
                    <?php //guaraci\authors::display($author_taxonomy); 
                    ?>

                    <?php if ($show_date) : ?>
                        <span class="date">
                            <?= '| Publicado em ' . get_the_date() ?>
                        </span>
                    <?php endif; ?>

                </span>
            <?php endif ?>
        <?php endif; ?>

        <?php if ($show_excerpt) : ?>
            <div class="card--excerpt"><?php echo ods\get_excerpt(); ?></div>
        <?php endif; ?>
    </div>

</div>